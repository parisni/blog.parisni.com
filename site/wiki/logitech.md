  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>
<span class="w3-tag w3-red tag">music</span>

# Logitech Media Server

## Installation

[Logitech provide a docker file](https://github.com/Logitech/slimserver-platforms/tree/public/8.0/Docker)

The docker compose way is the best:

```
version: '3'
services:
  lms:
    container_name: lms
    image: lmscommunity/logitechmediaserver
    volumes:
      - /<somewhere>:/config:rw
      - /<somewhere>:/music:ro
      - /<somewhere>:/playlist:ro
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
    ports:
      - 9000:9000/tcp
      - 9090:9090/tcp
      - 3483:3483/tcp
      - 3483:3483/udp
    environment:
      - PUID=1000
      - PGID=1000
    restart: always
```

Then with systemd it is easy to make it start at system startup.

```
[Unit]
Description=Logitech Media Server Daemon
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/<path to dockerfile>/
ExecStart=/usr/bin/env docker-compose up -d
ExecStop=/usr/bin/env docker-compose down
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
```

```
systemctl enable logitech
systemctl start logitech
docker ps
CONTAINER ID   IMAGE                              COMMAND                  CREATED          STATUS             PORTS                                                                                            NAMES
93b29a299406   lmscommunity/logitechmediaserver   "start-container"        44 minutes ago   Up 42 minutes      0.0.0.0:3483->3483/tcp, 0.0.0.0:9000->9000/tcp, 0.0.0.0:9090->9090/tcp, 0.0.0.0:3483->3483/udp   lms
```

## Security

It is possible to add [a basic
auth](http://squeezeplayer.de/2011/05/3g-part-3-secure-your-server/). It
is however a weak protection.


## Firewall

This will restrict access to LMS ports to local network only.

```bash
ufw delete from 192.168.1.0/16 to any app "LMS Full"
```

```bash
cat /etc/ufw/applications.d/lms-webserver 
[LMS Full]
title=Logitech Media Player
description=Logitech Media Player
ports=9000,9090,3483/tcp
```

