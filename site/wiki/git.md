  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">versionning</span>

# Git

## Branches

### Récupérer un fichier de puis une autre branche [source](https://stackoverflow.com/a/21718540)

```bash
# cherche depuis la branche distante server le fichier bookmark
git checkout origin/server   -- .w3m/bookmark.html
```


### Revenir a un commit

Si j'ai fait fausse route et que j'ai poussé les modif en ligne, je
peux revenir en arriere et faire de ce commit la version actuelle.


```bash
#retourner au commit f359
git revert --no-commit f359b96a1e1e680cf76ea06094e112b92b936b5e..HEAD
git commit
```

### Explorer les données d'un commit

```bash
#retourner au commit f359
git checkout f359b96a1e1e680cf76ea06094e112b92b936b5e
# puis par exemple pour retourner dans la branche master
git checkout master
```

### cesser de tracker un dossier

Ajouter au .gitignore le dossier. Puis lancer la commande:

```bash
git rm -r --cached <le-dossier>
```
cela ne supprimera pas le dossier

### Annuler les modifs d'un fichier

```bash
git checkout -- fichier-a-annuler-les-modif
```

### Revenir à un certain commit pour un certain fichier

```bash
git checkout <commit id> -- fichier-a-annuler-les-modif
```

### Rebase la branche master

```bash
git checkout master
git fetch --all #cf https://www.reddit.com/r/git/comments/8j70vv/should_i_fetch_before_pull/
git pull
git checkout branche-de-travail
git rebase master # evite d'avoir les commit de master par dessus son history; ils seront mis avant les modifs de la branche
```


### show history of file

```bash
git log -p filename
# OR
git show -2 filename # means number of history commit
```

### git rename branch


this will rename the branch
```bash
git branch -m gitlab/dev gitlab-dev
```

### git push remote


```bash
# this allows to push on an other remote, on the associated branch
git push <other-remote> <branch-to-push>
# this allows to push on a specific remote branch git push <other-remote> <branch-to-push>:<other-branch-to-push-on>
```


### Supprimer des fichier dans un depot

```bash
# permet de supprimer le fichier spark-meta/target/spark-meta-0.0.1.SNAPSHOT-shaded.jar
# de tout l´historique
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch spark-meta/target/spark-meta-0.0.1-SNAPSHOT-shaded.jar' --prune-empty --tag-name-filter cat -- --all
```

### Renommer un vieux commit

En utilisant *magit*:
1. désactiver les éventuels git-hooks (dans .git/hooks/pre-commit)
1. *rebase* (`r`)
1. choisir *--preserve-merge*  (`-p`)
1. choisir *rename commit* ( `w` )
1. choisir `C-c C-c` et renommer le commit
1. éventuellement, gérer les conflits, puis *continuer le rebase*


### Rebase et priorité sur des conflits

Par defaut, le rebase choisit les fichiers dans la branche
**yours**. Il va écraser des éventuelles modifications locales. On
peut changer ce comportement par defaut:

```
git rebase -X ours <branch>
```


### Récupérer une branche remote en local

```bash
git checkout --track <remote>/<branch>
```

### Remove submodule

```bash
submodule="folder/submodule"

git rm "$submodule"
rm -rf ".git/modules/$submodule"
git config -f ".git/config" --remove-section "submodule.$submodule" 2> /dev/null

# Commit the change
git commit -m "Remove submodule $submodule"

# Restart the state
git submodule deinit -f .
git submodule update --init
```

### Undo local commit

```
git reset HEAD~1 # where 1 is the number of commit to resume
```


### Undo several local commit and remove local files
```
git reset --hard <commit-id>
```

## bash prompt

1. Add https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh to ~/.git-prompt.sh

2. add this to .bashrc

```bash
if [ -f ~/.git-prompt.sh ]; then
. ~/.git-prompt.sh
GIT_PS1_SHOWDIRTYSTATE=true
GIT_PS1_SHOWCOLORHINTS=true
GIT_PS1_UNTRACKEDFILES=true
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]$(__git_ps1 " (%s)")\$ '
fi

```

## Show difference between two tags

```
git diff tag1 tag2

# for only one file
git diff tag1 tag2 -- some/file/name
```

## Delete all branch but master

```
git branch | grep -v "master" | xargs git branch -D
```

## Delete a tag

```bash
# local
git tag --delete 1.6.3
# remote
git push --delete origin <tag>
```

## Restore a file or path from a commit

```bash
git restore --source=<commit-id> path/to/thing
```

## Blame omit commits

```bash
# to get the full sha1
git blame --line-porcelain path/to/file

# then omit it by copy/paste sha1 into file
git blame --ignore-revs-file=.git-blame-ignore-revs path/to/file

```

## Restore a stash file

```
git diff stash^! -- <filename> | git apply
```
