  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">ide</span>
<span class="w3-tag w3-red tag">editor</span>

# IntelliJ

## Git Integration

### Git reviews
The github/gitlab reviews are painful / heterogeneous. The tool allows to get
fetch the PR and review the code easily.

## Vim Integration

`Vimidea`  let you use vim within intellij. To my experience, it si incredibly
well integrated. Some features are also better integrated. (for examble,
readline shortcuts within the comand bar)

## Reformat SQL on selected rows

```
:!pg_format
```

### Vim plugins

You can use [several vim plugin](https://github.com/JetBrains/ideavim/wiki/Emulated-plugins#highlightedyank)

~/.ideavimrc
```
Plug 'machakann/vim-highlightedyank'

set highlightedyank

Plug 'tpope/vim-surround'
set surround

set hlsearch
```
