  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>
<span class="w3-tag w3-red tag">security</span>

# UFW

```
ufw default deny incoming
ufw default allow outgoing
ufw allow SSH
ufw limit SSH
ufw allow "WWW Full"
ufw allow from 192.168.202.0/24 to any app "POSTGRESQL"
ufw enable

# show active:
ufw status numbered
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] 22                         ALLOW IN    Anywhere                  
[ 2] SSH                        ALLOW IN    Anywhere                  
[ 3] WWW Full                   ALLOW IN    Anywhere                  
[ 4] POSTGRESQL                 ALLOW IN    192.168.202.0/24
```
