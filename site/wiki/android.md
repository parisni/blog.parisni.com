  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">phone</span>

# Android

## termux
### Access to shared folder
```bash
termux-setup-storage
```
[from stack](https://android.stackexchange.com/a/185949)

## pdf/epub reader
### EbookDroid 
It makes a fantastic job... without trackers or adds
