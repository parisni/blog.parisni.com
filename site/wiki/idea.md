
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">editor</span>

# Generate javadoc

You can use the action `Fix doc comment`. It doesn't have a default
shortcut, but you can assign the Alt+Shift+J shortcut to it in the
Keymap.
