
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">web</span>
<span class="w3-tag w3-red tag">database</span>

# Prometheus


Prometheus can get metrics from system by pulling informations or by
making them push informations to a gateway. The grafana interface can
be plugged on the prometheus interface.

Overall config:

```
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
    - targets: ['localhost:9090']
  - job_name: 'node-exporter'
    static_configs:
    - targets: ['localhost:9100']
  - job_name: 'matrix'
    metrics_path: "/_synapse/metrics"
    static_configs:
      - targets: ['localhost:9101']
```

## Installation

On archlinux, install `prometheus` and `grafana` packages. Both have a
systemd service, on ports `9090` and `3000`. The config files are
`/etc/prometheus` and `/etc/grafana.ini`.


## Use timescaledb

Timescaledb can replace the default database backend. The promscale
extension improves the performances.

## Linux Metrics

On archlinux, install `prometheus-node-exporter` package and activate
the service. It will provide a metric http endpoint on port
`9100`. Then configure prometheus to fetch the metrics. You can [import the grafana dashboard](https://grafana.com/grafana/dashboards/1860).

## Matrix

The grafana configuration json [can be found
here](https://github.com/matrix-org/synapse/blob/master/contrib/grafana/synapse.json).
Configure synapse `homeserver.yaml`:

```yaml
listeners:
  - port: 9101
    type: metrics
    bind_addresses: ['0.0.0.0']
enable_metrics: true
```


## Jitsi

- [This blog explains how to](https://blog.zwindler.fr/2020/06/08/superviser-votre-instance-jitsi-avec-prometheus-et-grafana/)
- [The jitsi documentation](https://github.com/jitsi/jitsi-videobridge/blob/master/doc/statistics.md)
- [The dashboard](https://grafana.com/grafana/dashboards/11925)

## Pleroma

- [This blog post](https://coffee-and-dreams.uk/tutorials/2019/11/06/monitoring-pleroma.html)
