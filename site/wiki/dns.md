  <div id="generated-toc"> </div>
  <hr>
<span class="w3-tag w3-red tag">network</span>

# DNS

## Unbound


### Add dns filter

[Notracking](https://github.com/notracking/hosts-blocklists/issues/308#issuecomment-728269408) maintain a tracker list.

modify unbound.conf:

```
        control-enable: yes
        control-use-cert: "no"
```


Add a systemctl timer:


/etc/systemd/system/notracking.service
```
[Unit]
Description=No tracking
After=network.target

[Service]
Type=simple

ExecStart=/usr/local/etc/unbound/update_blacklist.sh

#ExecStop=/bin/kill -9 ${MAINPID}
WorkingDirectory=/usr/local/etc/unbound/

NoNewPrivileges=true
PrivateTmp=true
ProtectHome=read-only
ProtectSystem=strict
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @memlock @module \
                  @mount @obsolete @privileged @reboot @resources @setuid \
                  @swap @raw-io

ReadOnlyPaths=/
ReadWritePaths=/usr/local/etc/unbound/

PrivateDevices=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes

[Install]
WantedBy=multi-user.target
```


/etc/systemd/system/notracking.timer

```
[Unit]
Description=Run notrackking daily

[Timer]
OnCalendar=*-*-* 7:55:00
Persistent=true

[Install]
WantedBy=timers.target
```

```bash
systemctl enable notracking.timer
systemctl start notracking.timer
```
