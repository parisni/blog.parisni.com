
<div id="refman"> <div id="refman-sidebar"> <div id="generated-toc"> 
</div> 
</div> <div id="refman-main">

<span class="w3-tag w3-red tag">security</span>
<span class="w3-tag w3-red tag">linux</span>

# Pubkeys

## converting for ssh
```
---- BEGIN SSH2 PUBLIC KEY ----
Comment: "rsa-key-20200324"
AAAAB3NzaC1yc2EAAAAQEApWmENmDe+CcHplywBJQAAAiBYbZ8nmSeQA3taleMPR
P8VMt7mdUWhN5d6lAOw9UR/bfqrx1XkkWLVL57OWCjbxrMyWMKZBL2ZAGn8Z+cDO
qlldSEdBStGVUQ4gW2XLQE9gXKfWOVapxwbckcIZuNwwJrQpe+wWwp2L8zGRGO+C
sgzh9FGC1/XMlLkIV3A9uRWnL6qD4aMCDkk+TIGJS5kP9UB0GEHvEU2vhFCoI8u+
xaAvAHd49fzUok1Q//H/Dxp1XMKsIyIw1fGOQqvcmx0iymX9C5gGHEBEw+3EBE0I
tDZxPKRwD9aKu9zRvwkeYGSnXDgMiTCffmV4VcVcAN104n8D0Q==
---- END SSH2 PUBLIC KEY ----

```


This will flatten the key:
```

ssh-keygen -f theSsh2PubKey -i > key.pub
```

## Change passphrase

```
ssh-keygen -p -f ~/.ssh/id_rsa
# if pass = empty then no pass is needed
```


