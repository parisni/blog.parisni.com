  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">database</span>

# Mariadb

## Réaliser un dump simple
```bash
mysqldump database > /tmp/backup-file.sql
```
## Charger un dump simple
```bash
mysql database < /tmp/backup-file.sql
```
