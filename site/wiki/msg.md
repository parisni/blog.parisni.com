
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">messaging</span>
<span class="w3-tag w3-red tag">security</span>

# Encrypted Messaging Apps
Only the FOSS applications are listed here. The closed-source cannot
be trusted for privacy.

## Matrix
- federated, no need for central server
- group conversation
- IOS / Android / Desktop / Web

## Session
- [self hosting](https://github.com/oxen-io)
- signal fork
- no phone number but an alphanumeric
- handle async messaging
- group conversation
- NO voice call
- IOS / Android / Desktop
- Multiple device can be linked

## Wire
- [self hosting possible](https://github.com/wireapp/wire-server)
- cofunded by skype creator
- designed to scale on kubernetes
- group conversation
- video
- voice call


## Deltachat
- distributed, no need for central server
- group conversation
- based on email
- encrytion with pgp
- metadata leaks in the mail
- IOS / Android / Desktop
- Multiple device can be linked
- message you send are not visible on each device

## Jami
- distributed, no need for central server
- peer-to-peer
- group conversation
- NO async message
- Multiple device can be linked
- message you send are not visible on each device

## Snikket
- based on XMPP

## Tox
- distributed, no need for central server
- peer-to-peer
- NO async message
