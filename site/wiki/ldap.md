  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">security</span>

# Ldap

## Start a ldap server

```bash
docker run -p 389:389 -p 636:636 --volume /data/slapd/database:/var/lib/ldap  --name my-openldap-container --detach osixia/openldap:latest
```


## Search into a ldap server

```bash
docker exec my-openldap-container ldapsearch -x -H ldap://0.0.0.0:389 -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w admin
```

## Add a user

```bash
docker exec my-openldap-container ldapadd -x -D "cn=admin,dc=example,dc=org" -w admin -f /container/service/slapd/assets/test/new-user.ldif -H ldap://0.0.0.0:389
```

## Delete a user

```bash
docker exec openldap ldapdelete -x -D "cn=admin,dc=example,dc=org" -w admin "uid=nico,dc=example,dc=org" -H ldap://0.0.0.0:389
```

## Generate a password

The salt is contained in the result, so multiplle call returns multiple results.

```bash
# into the docker container
slappasswd -h {SSHA} -s abcd123
```


## Great tools

- [ldap password webui](https://github.com/npenkov/ldap-passwd-webui)

```yaml
  webui:
    image: npenkov/docker-ldap-passwd-webui:latest
    ports:
      - "8081:8080"
    environment:
      LPW_TITLE: "Change your global password for example.org"
      LPW_HOST: "openldap"
      LPW_PORT: "389"
      LPW_ENCRYPTED: "false"
      LPW_START_TLS: "false"
      LPW_SSL_SKIP_VERIFY: "true"
      LPW_USER_DN: "uid=%s,dc=example,dc=org"
      LPW_USER_BASE: "dc=example,dc=org"
      LPW_PATTERN: '.{8,}'
      LPW_PATTERN_INFO: "Password must be at least 8 characters long."
```
