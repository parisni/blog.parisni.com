<div id="refman"> <div id="refman-sidebar"> <div id="generated-toc"> </div> </div> <div id="refman-main">

<span class="w3-tag w3-red tag">email</span>
<span class="w3-tag w3-red tag">emacs</span>

# Mu4e

Important de mettre en place la fonction "pinentry emacs" pour que le
mot de passe soit demandé (dans le cas d´emacs-terminal mode)

## Crypt / Decrypt

Installer les paquets:
- pinentry

Editer le fichier `.gnupg/gpg-agent.conf` pour ajouter :

```
pinentry-program /usr/bin/pinentry-emacs
default-cache-ttl 9999
max-cache-ttl 9999
allow-emacs-pinentry
```

Redemarrer `gpg-agent`:

IMPORTANT: cela ne fonctionne que si une instance d´emacs est en cours (pas avec emacsclient)

```bash
pkill gpg-agent
```

Attention, toute utilisation de gpg ulterieure demandera dans emacs et
non dans le tty !

## Sidebar

Le paquet `emacs-overview` permet d´afficher la liste des sous-maildir
de manière identique à mutt. Il faut penser à raffraichir les comptes
(par exemple: <cc> <cu>).

Cela fonctionne bien à condition d´avoir ajouté une seconde windows
dans laquelle afficher le contenu des maildir.

## Draft

### Ajourner un email
  
<C-c> <C-d> envoie l'email dans les drafts.

### Reprendre un email draft

<c e> permet de ré-éditer un email

### Gestion des listes

The 1.3.x mu4e version provide an answer list feature.

## Marks

``` txt
mark for/as  | keybinding  | description
-------------+-------------+------------------------------
'something'  | *, <insert> | mark now, decide later
delete       | D, <delete> | delete
flag         | +           | mark as 'flagged' ('starred')
move         | m           | move to some maildir
read         | !           | mark as read
refile       | r           | mark for refiling
trash        | d           | move to the trash folder
untrash      | =           | remove 'trash' flag
unflag       | -           | remove 'flagged' mark
unmark       | u           | remove mark at point
unmark all   | U           | remove all marks
unread       | ?           | marks as unread
action       | a           | apply some action
```

Then use `x` to apply any mark

## Next / Previous unread message

- `[ [`: next
- `] ]`: previous
