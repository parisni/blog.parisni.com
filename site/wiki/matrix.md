
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">conversation</span>

# Matrix

## Install your own server


[matrix server](https://wiki.archlinux.org/index.php/Matrix)

[documentation](https://matrix.org/docs/guides/#installing-synapse)

[ubuntu detail](https://www.natrius.eu/dokuwiki/doku.php?id=digital:server:matrixsynapse)

## Reset a user password

[this can be done in the database](https://paritoshbh.me/blog/reset-user-password-synapse-matrix-homeserver)


## Connect to IRC libera channel

To connect to `#channel` just join `#channel:libera.chat`


## Upgrade a room

This will make the current room read-only, and create a new room [see
rfc](https://github.com/matrix-org/matrix-doc/blob/main/proposals/1501-room-version-upgrades.md).


Simply type this as a message (you will be asked to invite every participant) :

```
/upgraderoom 7
```

Run an api request :

```bash
curl -H 'Authorization: Bearer <token-access>' -H "Content-Type: application/json"  -X POST https://matrix.interhop.org/_matrix/client/r0/rooms/<room-id-url-encoded>/upgrade -d '{"new_version": "6"}'
```
- token-access: In element > All settings > Help about > Access Token (bottom page)
- room-id: In element > Room Info
