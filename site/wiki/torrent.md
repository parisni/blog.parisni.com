  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">torrent</span>
<span class="w3-tag w3-red tag">p2p</span>

# Transmission

## Magnet link
1. get the info-hash of the file
2. transform it into magnet link at [info-hash to magnet link](http://romanr.info/magnet.html)
3. run `transmission-cli -w <destination/folder> "<magnet link content>"`

## Active website
- [torrent411](https://www2.yggtorrent.si/) accessible from tor-browser
- [the pirate bay](https://thepiratebay.org) accessible from tor-browser


## Personal Web interface

[see the docker application](https://github.com/linuxserver/docker-transmission)

## Blocklist

- [up-to-date blacklist](https://www.iblocklist.com/list?list=fr)
- ```
  # edit settings.json
  "blocklist-enabled": true,
  "blocklist-url": "http://list.iblocklist.com/?list=fr&fileformat=p2p&archiveformat=gz",
  transmission-remote host:port -n admin:password --blocklist-update
  ```
- there is no need for updating since docker-transmission takes care of it [see the issue](https://github.com/linuxserver/docker-transmission/issues/14)
