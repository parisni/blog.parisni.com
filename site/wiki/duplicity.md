
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">backup</span>

# Duplicity

## Configure s3 access
```
# Export some ENV variables so you don't have to type anything
export AWS_ACCESS_KEY_ID=<the access key>
export AWS_SECRET_ACCESS_KEY=<the secret key>
export PASSPHRASE=<the gpgkey password>
```

## Backup an encrypted directory on s3
```bash
#!/usr/bin/env bash
#set -e

# Your GPG key
GPG_KEY=<the-gpg-key>

SOURCE=/
# The S3 destination followed by bucket name
DEST="s3://the-bucket"

duplicity \
    --full-if-older-than 1M \
    --encrypt-key=${GPG_KEY} \
    --sign-key=${GPG_KEY} \
    --include=/opt \
    --exclude=/** \
    ${SOURCE} ${DEST}

duplicity remove-all-but-n-full 1 --force  ${DEST}
```

## Restore the backup
This will restore the lastest state of the backup. Apparently, it is also
possible to play with time `--restore-time`.

```bash
SOURCE="s3://the-bucket"
DEST=/mnt/backup/mailu

duplicity \
    restore \
    --encrypt-key=${GPG_KEY} \
    --sign-key=${GPG_KEY} \
     ${SOURCE} ${DEST}
```
