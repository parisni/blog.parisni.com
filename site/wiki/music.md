  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">music</span>

# Music
## Extract music from mkv
[source](https://www.hecticgeek.com/extract-audio-using-ffmpeg-ubuntu/)

you can then convert the “mka” into flac, or ogg with vlc-media-player.

## Edit id3-tags

Use [music-tag](https://github.com/KristoforMaynard/music-tag)

```bash
python -m music_tag --to-csv tags.csv ./sample
# Edit the csv and re-import it to apply the id3
python -m music_tag --from-csv tags.csv
```
