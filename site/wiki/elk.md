  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">monitoring</span>

# ELK

## Filebeat setup

Modules can be enabled:

```bash
filebeat modules enable system nginx postgresql
```

So far, only direct access to elastic works with filebeat:

```bash
filebeat --path.home /usr/share/filebeat   --path.config /etc/filebeat   --path.data /var/lib/filebeat   --path.logs /var/log/filebeat setup -e
```
