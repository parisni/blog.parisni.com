  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">web</span>

# Web Browser

## Ungoogled-Chromium

It is not possible to install extensions thought the chrome store. You
need to install them manually:

`https://clients2.google.com/service/update2/crx?response=redirect&acceptformat=crx2,crx3&prodversion=[VERSION]&x=id%3D[EXTENSION_ID]%26installsource%3Dondemand%26uc`

- VERSION: for example 84.0
- EXTENSION_ID: the hash can be found on the extension url


It has several cons:
- cannot change default shortkeys (like any chromium based browser but vivaldi)
- cannot install extensions easily

## Firefox

It has several cons:
- cannot change default shortkeys


## Vivaldi

It has several cons:
- proprietary software

## Qutebrowser

https://qutebrowser.org/doc/install.html#tox

pros:
- fairly good adblocker
- readline shortkeys in both command / input
- minimalist


cons:
- does not discover all pleroma links

### Search with “startpage”
This provides a GET search in clear
```python
c.url.searchengines = {'DEFAULT': 'https://www.startpage.com/sp/search?query={}&prfh=disable_open_in_new_windowEEE0N1Ndisable_video_family_filterEEE1N1Nenable_post_methodEEE0N1Nenable_proxy_safety_suggestEEE1N1Nenable_stay_controlEEE0N1Ngeo_mapEEE0N1Nlang_homepageEEEs%2Fblak%2Ffr%2FN1NlanguageEEEenglishN1Nlanguage_uiEEEfrancaisN1Nnum_of_resultsEEE10N1Nother_iaEEE1N1Nsearch_results_regionEEEallN1NsuggestionsEEE0N1Nwikipedia_iaEEE1N1Nwt_unitEEEcelsius&language=english&t=blak&lui=francais&cat=web&sc=wb8Rggx5esKu20&abp=-1'}

```

### Disable HTS on chromium

- https://www.thesslstore.com/blog/clear-hsts-settings-chrome-firefox/


## Vimium-c (firefox & chromium)


Together with gtk-emacs binding only those are necessary:

```
# delete backward character
mapkey <c-b:i> <f3rmc>
map <f3rmc> editText run="move,backward,character,exec,move,"

# delete backward character
mapkey <c-k:i> <f3rfor>
map <f3rfor> editText run="extend,forward,lineboundary,exec,delete,"

# delete backward character
mapkey <c-u:i> <f3rback>
map <f3rback> editText run="extend,backward,lineboundary,exec,delete,"
```


Add mapkey :

```
mapkey <a-d:i> <f3rmf>
map <f3rmf> editText run="extend,forward,word,exec,delete,"

# delete forward character
mapkey <c-d:i> <f3rmcf>
map <f3rmcf> editText run="extend,forward,character,exec,delete,"

# delete backward word
mapkey <a-backspace:i> <f3rm>
map <f3rm> editText run="extend,backward,word,exec,delete,"

# delete backward character
mapkey <c-h:i> <f3rmc>
map <f3rmc> editText run="extend,backward,character,exec,delete,"

mapkey <c-e:i> <f3end>
map <f3end> editText run="move,forward,lineboundary,exec,move,"

mapkey <a-f:i> <f3wend>
map <f3wend> editText run="move,forward,word,exec,move,"

mapkey <a-b:i> <f3wbig>
map <f3wbig> editText run="move,backward,word,exec,move,"

# not used because mapped to select text. use home instead
# mapkey <c-a> <f3bg>
# map <f3bg> editText run="move,backward,lineboundary,exec,move,"

mapkey <c-k:i> <f3rmline>
map <f3rmline> editText run="extend,forward,lineboundary,exec,delete,"
mapkey <c-n:i> <s-down>
mapkey <c-p:i> <s-up>
```

## Chromium


### Install chromium

Install [without snap on ubuntu 20.04](https://askubuntu.com/a/1206153) to make
browser-pass (password-store) working.

- install the extension
- also install the `sudo apt install webext-browserpass`
