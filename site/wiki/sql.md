  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">sql</span>
<span class="w3-tag w3-red tag">emacs</span>

# Sql Clients

## Orgmode SQL

[That post](https://orgmode.org/worg/org-contrib/babel/languages.html) introduced me the org-mode sql. I find it convenient to get the result within an org notebook. However still, I have not found equivalent to psql shortcuts such `\d` to list all tables, and so on. 

```lisp
;; https://orgmode.org/worg/org-contrib/babel/languages.html
;; active Babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((sql . t)
   (emacs-lisp . nil)))
```

```

#+begin_src sql :engine postgresql :dbhost localhost :dbuser myuser:database thedb :dbport theport
select "savedAt", substring("content" ,1 , 30) b
 from "Notes" 
 limit 3 
#+end_src 

#+RESULTS:
| savedAt                    | b                             |
|----------------------------+-------------------------------|
| 2019-11-05 09:58:43.363+01 |                               |
| 2019-11-02 14:46:27.037+01 | installé sur la db de natus > |
| 2019-11-02 15:26:33.183+01 |                               |
```
