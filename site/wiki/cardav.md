  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">Contacts</span>

# Contacts

## Radicale

This is a cardav server to selfhost contacts and tasks. It comes bundled with
mailu.

## Vdirsyncer

The tool allows to sync a remote cardav to local. This can then be used by
tools such khard, which in turn can be used in aerc.

### Configure

```
[general]
status_path = "~/.vdirsyncer/status/"

[pair my_contacts]
a = "my_contacts_local"
b = "my_contacts_remote"
collections = ["from a", "from b"]

[storage my_contacts_local]
type = "filesystem"
path = "~/.contacts/"
fileext = ".vcf"

[storage my_contacts_remote]
type = "carddav"

# We can simplify this URL here as well. In theory it shouldn't matter.
url = "https://<domain>/webdav/<username>"
username = "<username>"
password = "<password>"
```

### Sync local and remote

```bash
vdirsyncer discover my_contacts
vdirsyncer sync
```

## Khard

Khard is a cli manager for contacts. It integrates well with aerc mailing client.

### Add a new contact

```bash
khard new --open-editor
# then add at leat organisation and formatted name within $EDITOR
```


## Aerc configuration

*~/.config/aerc/aerc.conf*

```
# Specifies the command to be used to tab-complete email addresses. Any
# occurrence of "%s" in the address-book-cmd will be replaced with what the
# user has typed so far.
address-book-cmd=khard email --parsable --search-in-source-files --remove-first-line %s
```
