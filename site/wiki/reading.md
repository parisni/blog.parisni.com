<div id="refman"> <div id="refman-sidebar"> <div id="generated-toc"> </div> </div> <div id="refman-main">

<span class="w3-tag w3-red tag">books</span>
<span class="w3-tag w3-red tag">method</span>

# Reading

From Bernard Stiegler, applied for 5 years in prison:

1. wake up: poesie (stefane malarmé)
2. phylosophy until lunch
3. synthesis of morning reading
4. reading of synthesis of previous day
5. reading of marcel proust
