  <div id="generated-toc"> </div>
  <hr>
<span class="w3-tag w3-red tag">email</span>
<span class="w3-tag w3-red tag">linux</span>

# Evolution

Install it with flatpak. See [ubuntu flatpak](../ubuntu/#flatpak). So far, this
is the most reliable way and it has uptodate installed packages

## Installation


The below procedure adds emacs key-binding and dark mode for evolution installed with flatpak.

```bash
flatpak install flathub org.gnome.Evolution
flatpak install org.gtk.Gtk3theme.Yaru-dark
cp .config/gtk-3.0/settings.ini /home/nparis/.var/app/org.gnome.Evolution/config/gtk-3.0/
cp .config/gtk-3.0/gtk.css /home/nparis/.var/app/org.gnome.Evolution/config/gtk-3.0/
```

```bash
cat .config/gtk-3.0/settings.ini
[Settings]
gtk-application-prefer-dark-theme=true
gtk-key-theme-name = Emacs
gtk-icon-theme-name = "Yaru-dark"
gtk-can-change-accels = 1
```

You can edit the gtk emacs key bindings here (added alt-backspace)

```bash
cat .config/gtk-3.0/gtk.css
@binding-set gtk-emacs-text-entry
{
  bind "<ctrl>b" { "move-cursor" (logical-positions, -1, 0) };
  bind "<shift><ctrl>b" { "move-cursor" (logical-positions, -1, 1) };
  bind "<ctrl>f" { "move-cursor" (logical-positions, 1, 0) };
  bind "<shift><ctrl>f" { "move-cursor" (logical-positions, 1, 1) };

  bind "<alt>b" { "move-cursor" (words, -1, 0) };
  bind "<shift><alt>b" { "move-cursor" (words, -1, 1) };
  bind "<alt>f" { "move-cursor" (words, 1, 0) };
  bind "<shift><alt>f" { "move-cursor" (words, 1, 1) };

  bind "<ctrl>a" { "move-cursor" (paragraph-ends, -1, 0) };
  bind "<shift><ctrl>a" { "move-cursor" (paragraph-ends, -1, 1) };
  bind "<ctrl>e" { "move-cursor" (paragraph-ends, 1, 0) };
  bind "<shift><ctrl>e" { "move-cursor" (paragraph-ends, 1, 1) };

  bind "<ctrl>w" { "cut-clipboard" () };
  bind "<ctrl>y" { "paste-clipboard" () };

  bind "<ctrl>d" { "delete-from-cursor" (chars, 1) };
  bind "<alt>d" { "delete-from-cursor" (word-ends, 1) };
  bind "<ctrl>k" { "delete-from-cursor" (paragraph-ends, 1) };
  bind "<alt>backslash" { "delete-from-cursor" (whitespace, 1) };

  bind "<alt>space" { "delete-from-cursor" (whitespace, 1)
                      "insert-at-cursor" (" ") };
  bind "<alt>KP_Space" { "delete-from-cursor" (whitespace, 1)
                         "insert-at-cursor" (" ")  };
  /*
   * Some non-Emacs keybindings people are attached to
   */
  bind "<ctrl>u" { "move-cursor" (paragraph-ends, -1, 0)
                   "delete-from-cursor" (paragraph-ends, 1) };

  bind "<ctrl>h" { "delete-from-cursor" (chars, -1) };
  bind "<ctrl>w" { "delete-from-cursor" (word-ends, -1) };
  bind "<alt>BackSpace" { "delete-from-cursor" (word-ends, -1) };
}
```


Uncomment the accels entries and override the Evolution key-map. This shall be
done when Evolution is not running, otherwise the editions will be lost after
restart:

```bash
cat ~/.var/app/org.gnome.Evolution/config/evolution/accels
; evolution.bin GtkAccelMap rc-file         -*- scheme -*-
; this file is an automated accelerator map dump
(gtk_accel_path "<Actions>/mail/mail-next" "j")
(gtk_accel_path "<Actions>/mail/mail-print" "")
(gtk_accel_path "<Actions>/mail/mail-previous" "k")
```

## Configuration

- Preference > Mail Preference > Plain Text Mode > Show Plain Text If Present
- View > Preview > Vertical View
