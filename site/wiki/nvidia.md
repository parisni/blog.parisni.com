  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">gpu</span>

# Nvidia

## Désactiver la carte nvidia

```bash
sudo /usr/bin/prime-select nvidia
sudo /usr/bin/prime-select intel
```
