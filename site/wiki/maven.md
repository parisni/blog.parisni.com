
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">java</span>
<span class="w3-tag w3-red tag">scala</span>

# Maven

## Download artefacts in dedicated repository

Some software (such spark) need to have jars in local. Maven is a nice
way to synchronize nexus and a given repository.

This maven plugin allow to export the dependencies into the
`outputDirectory`. Moreover it can strip the version.


```xml
        <profile>
            <id>default</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <build>
                <pluginManagement>
                    <plugins>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-dependency-plugin</artifactId>
                            <version>3.1.2</version>
                            <executions>
                                <execution>
                                    <id>get-package</id>
                                    <phase>package</phase>
                                    <goals>
                                        <goal>copy-dependencies</goal>
                                    </goals>
                                    <configuration>
                                        <outputDirectory>${project.build.directory}/dependencies/</outputDirectory>
                                        <stripVersion>true</stripVersion>
                                    </configuration>
                                </execution>
                            </executions>
                        </plugin>
                    </plugins>
                </pluginManagement>
            </build>
        </profile>
```

It is then possible to specify the lastest version of a given
dependency and produce a striped version accessible from a given
folder.

```xml
    <properties>
	    <spark-etl.version>[1.0.10,)</spark-etl.version>
        <omop-spark.version>[1.0.1,)</omop-spark.version>
    </properties>
	
	    <dependencies>

        <dependency>
            <groupId>io.frama.parisni</groupId>
            <artifactId>spark-csv</artifactId>
            <version>${spark-etl.version}</version>
            <classifier>shaded</classifier>
            <exclusions>
                <exclusion>
                    <groupId>*</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

	    </dependencies>
```

The dependency can be produced by using the below command:

```bash
mvn -P "default,snapshot" package \
-Dmaven.wagon.http.ssl.insecure=true \ # those are used because of ssl problems on my side...
-Dmaven.wagon.http.ssl.allowall=true \
-Dmaven.wagon.http.ssl.ignore.validity.dates=true
```
