  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">database</span>

# Solr

## Gestion des dates

Dans solr, les dates sont stoquées en UTC. Cela a des implications complexes pour le client:
- transformer les dates extraites de solrj dans la timezone du client
- lorsque des filtres temporels sont appliqués il faut normaliser les dates en UTC

## Gestion des node

Pour changer de configuration (nombre de shard, port...) une instance, je n'ai pas d'autre option que de réinstaller solr.


## inner joins

```
fq={!join from=patient to=id fromIndex=list score=none v='type:400000'} AND name:bob
```

In order to join a sharded collection on solr-cloud, the joined
collection must be replicated on every nodes.

In terms of performances, adding `score=none` allow to make the search
search proportional to the joined number of rows. Without this, it
will be stable and only depend on the collection size.

Interestingly, if the join is used in `fq` then it is cached by
solr. Then any new query on the collection with the same inner join
will be faster.
