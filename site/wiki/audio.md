
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">audio</span>

# Audio

## Télécharger une emission sur France Culture

Testé avec chromium:
1. écouter l'émission
1. clic droit sur le bandeau de lecture
1. inspecter l'élément
1. rechercher dans le source “mp3”
1. récupérer le lien pour télécharger

# Transformer du webm en ogg

```bash
ffmpeg -i file.webm -vn -ab 128k -ar 44100 -y file.ogg
```

# Show id3 tags

```bash
ffprobe -hide_banner -show_entries format_tags=artist,title 02\ Snake\ Hips.mp3 | sed 's/\[[^]]*\]//g' | cut -d "=" -f 2
[mp3 @ 0x5640e4f4e320] Estimating duration from bitrate, this may be inaccurate
Input #0, mp3, from '02 Snake Hips.mp3':
  Metadata:
    title           : Snake Hips
    album           : Far Out Son Of Lung And The Ra
    comment         :
    publisher       : Astralwerks
    track           : 2
    compilation     : 1
    album_artist    : The Future Sound of London
    genre           : Electronica
    composer        : The Future Sound of London
    artist          : The Future Sound Of London
    date            : 1994
  Duration: 00:08:33.82, start: 0.000000, bitrate: 320 kb/s
    Stream #0:0: Audio: mp3, 44100 Hz, stereo, s16p, 320 kb/s

Snake Hips
The Future Sound Of London
```
