  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-orange tag">music</span>
<span class="w3-tag w3-orange tag">haskell</span>
<span class="w3-tag w3-orange tag">emacs</span>

# Tidal

## Installation

### installation de supercollider

```bash
nix-env -i supercollider
```

Il est alors possible de le lancer via:

```bash
$> scide
```

### installation de superdirt

```
Quarks.checkForUpdates({Quarks.install("SuperDirt", "v1.1.1"); thisProcess.recompile()})                                                                            
SuperDirt.start                                                                                                                                                     
# produit cette erreur...
# Booting server 'localhost' on address 127.0.0.1:57110.
# could not initialize audio.
# Server 'localhost' exited with exit code 0.
```

### installation de tidal emacs

```bash
nix-env -iA nixos.haskellPackages.tidal
```

Ajouter dans init.el:

```lisp
;; tidal
(add-to-list 'load-path "~/.emacs.d/plugins/tidal/")
(require 'tidal)
(require 'haskell-mode)
```
