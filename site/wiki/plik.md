  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">wetransfert</span>
<span class="w3-tag w3-red tag">files</span>
<span class="w3-tag w3-red tag">self-host</span>

# Plik

It has interessting features such: s3 support, server side encryption (s3
only), restricted access to users, powerfull multi-arch cli.


Equivalent tools:

- [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/-/blob/master/lufi.conf.template) [docker-image](https://github.com/Hamzelot/lufi-docker)
- [jirafeau](https://gitlab.com/mojo42/Jirafeau)
- [transfer.sh](https://github.com/dutchcoders/transfer.sh/)
- [securedrop](https://github.com/freedomofpress/securedrop)

## Docker Compose

Use postgres, restrict upload to created users

```toml
MaxTTLStr           = "0"              # 0 : No limit
ProtectedByPassword = true             # Allow users to protect the download with a password
NoWebInterface      = false            # Disable web user interface

Authentication      = true             # Enable authentication
NoAnonymousUploads  = true             # Prevent unauthenticated users to upload files

[MetadataBackendConfig]
    Driver = "postgres"
    ConnectionString = "postgres://<user>:<password>@<ip>:<port>/<db>"

```

## Server cli

```
./plikd user create --login <login> --name <username> --password <password> --email <email> --admin
```

## Server UI

- generate a token in order the cli client can connect

## Client

```
plik --update # init a .plikrc file
```
