<!--
  <div id="generated-toc"> </div>
  <hr>
  -->

<span class="w3-tag w3-red tag">web</span>
<span class="w3-tag w3-red tag">security</span>

# Certs

Let's encrypt provides free certificates. However, the configuration and maintenance is tedious.

## Certbot

[Certbot](https://certbot.eff.org/about/) is a blazing fast way of
configuring https on your website, independantly of os, web server
software.

