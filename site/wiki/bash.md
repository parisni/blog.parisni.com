  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>

# Bash

## remove files with chinese character AND spaces
- PCRE regexp with `-P`
- quote spaces with xargs + pattern

```bash
#./[机器学习]Introduction to Machine Learning.pdf
#./算法竞赛入门经典（第2版） (算法艺术与信息学竞赛).pdf

find . | grep -P '[\p{Han}]'|xargs -I '{}' rm "{}"
```


## remove space in filename


`prename` is a perl rewrite of rename. On archlinux its called
`perl-rename` and needs to be installed. Its prebuilt on debian:

``` bash
find . -name '*.mkv' -exec prename 's/ +/_/g' {} +
```

## Create empty files at minimum depth 4

```bash
find . -mindepth 4  -type d -exec touch {}/state.json \;
```

## create a securized user

```bash
useradd -m -G users -s /bin/bash <the user>
```

## run command for a securized user

```bash
sudo -Hu <the user> <the command>
```

## Git Prompt

Shows details about git project when in the folder

```
git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1

# add to bashrc
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source $HOME/.bash-git-prompt/gitprompt.sh
fi
```

## Improved ls

```
alias ll="exa -l --git --all --octal-permissions --sort modified --time-style long-iso --group --color=auto --color-scale"
```

## Edit a file without vi

```
cat - > /path/to/file
# paste the content
# then ctrl+D
```

## Create a tunnel
```bash
ssh -fNTML 9432:localhost:5432 sshusername@you-server.com
Then, just launch psql, connecting to port 9432 at localhost:

psql -h localhost -p 9432 -U <username> <dbname>
```

## Get file magic number

```bash
18:42 $ file -i assets/images/puml.*
assets/images/puml.png:  image/png; charset=binary
assets/images/puml.webp: image/webp; charset=binary
```

## Get last created folders in current path

```bash
find . -ctime -1|cut -d'/' -f2|sort -u
```

## Tar

### Tar from find

```bash
tar czf <file>.tgz `find . -name '*.json' -maxdepth 1`
```
### Tar from find

```bash
tar czf <file>.tgz <folder> --exclude=<path/to/*>
```

## Find exec multi args command

1. write a script in the path
2. make it executable
3. find . -exec ./script {} \;
