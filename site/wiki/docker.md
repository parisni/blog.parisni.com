  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">containers</span>

# Docker

## Default ip addresses

Set default docker address:

```
{
  "log-driver": "journald",
  "bip": "172.26.0.1/16",
  "default-address-pools": [
          {
                  "base": "172.254.0.0/16",
                  "size": 24
          }
  ]
}
```
