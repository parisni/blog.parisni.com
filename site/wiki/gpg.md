  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">security</span>

# Gpg

## Expired key

It is good practice to make key expire. In order to renew an expired key:
```bash
gpg --edit-key <the key ID>
# then choose tu subkey
key 1 # for the second subkey
expire
2y # for 2years
save

```

## Export a private key

```bash
gpg --output /tmp/thekey.secret-key --armor --export-secret-key <the key id>
```

## Import a private key

```bash
gpg --import thekey.secret-key
```
