  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">automation</span>

# Makefile

## Pass args to a command

The _filter-out_ command removes the former arg from the latter args.

`makefile` example:

```bash
# allows passing args to make file
# see https://stackoverflow.com/a/47008498/3865083
# use below command to get the args:
# $(filter-out $@,$(MAKECMDGOALS))
%:
	@:
action:
	echo "$(filter-out $@,$(MAKECMDGOALS))"
```

Then those will behave the same :

```bash
make action foo bar baz
# OR
make foo bar action
```

## Default goal

Add this:

```bash
.DEFAULT_GOAL := all
```

## Phony

By default, make will skip tasks when the current folder contains action names.
_Phony_ deactivate this feature.

```bash
.PHONY: all
```

## Instanciate variables

This way:

```bash
var := content
```
