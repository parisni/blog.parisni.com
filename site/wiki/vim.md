  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>
<span class="w3-tag w3-red tag">editor</span>

# Vim

## Install neovim

```bash
# this to be run at each release
wget --quiet https://github.com/neovim/neovim/releases/download/stable/nvim.appimage --output-document nvim
chmod +x nvim
sudo mv nvim /usr/bin/
sudo ln -s /usr/bin/{nvim,vim}
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim 1
sudo update-alternatives --config editor
```

Add those into .bashrc:
```
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim
```

## Install plugins

```bash
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

```bash
cat ~/.config/nvim/init.vim
set encoding=utf-8
colo murphy
set hlsearch
set nonu

call plug#begin()
Plug 'https://github.com/ryvnf/readline.vim.git'
Plug 'https://github.com/tpope/vim-surround.git'
call plug#end()
```

Plugin install/update:
```
:PlugInstall
:UpdateRemotePlugins
```

## Regexp

### Gready / non Gready

In vim,  `.\{-}` is equivalent to `.*?` for non gready operators:

```
:%s/jim.\{-}//g
```

## Delete blank lines

```
:g/^$/d
```

## No format on paste

```
:set paste
# Note: it breaks vim-readline
```

## No numbering

```
:set nonu
```

## Vim Surround

Add surround:

1. Visual a word, part of text
2. `S"` -> "surround"
3. `S(` -> (surround)

Replace surround:

1. On a "word" press `cs"'` to change to  'word'
1. On a "word" press `cs")` to change to  (word)

Delete surround:

1. On a "word" press `ds"` to change to word
