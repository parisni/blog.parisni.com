  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">email</span>

# Simpa

## Installation

Plusieurs dockerfiles existent, mais l´installation semble
obligatoirement interactive, ce qui rend les choses plus compliquées.
Les depots yaourt fournissent un paquet `sympa`.

### Base de données

Il faut penser à installer le package pacman `perl-dbd-pg` pour que la
connection à postgres se réalise.

### Sendmail

Pour avoir un sendmail fonctionnel, il faut penser à démarrer le
service. Il est possible de tester son fonctionnement:

``` bash
echo $HOSTNAME | /usr/bin/sendmail  -v natus@riseup.net

natus@riseup.net... Connecting to [127.0.0.1] via relay...
220 nc-ass-vip.sdv.fr ESMTP Sendmail 8.15.2/8.15.2; Sat, 15 Feb 2020 19:28:10 +0100
>>> EHLO nc-ass-vip.sdv.fr
250-nc-ass-vip.sdv.fr Hello localhost [127.0.0.1], pleased to meet you
250-ENHANCEDSTATUSCODES
250-PIPELINING
250-EXPN
250-VERB
250-8BITMIME
250-SIZE
250-DSN
250-ETRN
250-AUTH DIGEST-MD5 CRAM-MD5
250-DELIVERBY
250 HELP
>>> VERB
250 2.0.0 Verbose mode
>>> MAIL From:<natus@nc-ass-vip.sdv.fr> SIZE=15 AUTH=natus@nc-ass-vip.sdv.fr
250 2.1.0 <natus@nc-ass-vip.sdv.fr>... Sender ok
>>> RCPT To:<natus@riseup.net>
>>> DATA
250 2.1.5 <natus@riseup.net>... Recipient ok
354 Enter mail, end with "." on a line by itself
>>> .
050 <natus@riseup.net>... Connecting to mx1.riseup.net. via esmtp...
050 220 mx1.riseup.net ESMTP (spam is not appreciated)
050 >>> EHLO nc-ass-vip.sdv.fr
050 250-mx1.riseup.net
050 250-PIPELINING
050 250-SIZE 25600000
050 250-ETRN
050 250-STARTTLS
050 250-ENHANCEDSTATUSCODES
050 250-8BITMIME
050 250 DSN
050 >>> STARTTLS
050 220 2.0.0 Ready to start TLS
050 >>> EHLO nc-ass-vip.sdv.fr
050 250-mx1.riseup.net
050 250-PIPELINING
050 250-SIZE 25600000
050 250-ETRN
050 250-ENHANCEDSTATUSCODES
050 250-8BITMIME
050 250 DSN
050 >>> MAIL From:<natus@nc-ass-vip.sdv.fr> SIZE=303
050 250 2.1.0 Ok
050 >>> RCPT To:<natus@riseup.net>
050 >>> DATA
050 250 2.1.5 Ok
050 354 End data with <CR><LF>.<CR><LF>
050 >>> .
050 250 2.0.0 Ok: queued as 48KdzJ0tYhzFd8d
050 <natus@riseup.net>... Sent (Ok: queued as 48KdzJ0tYhzFd8d)
250 2.0.0 01FISAiT005455 Message accepted for delivery
natus@riseup.net... Sent (01FISAiT005455 Message accepted for delivery)
Closing connection to [127.0.0.1]
>>> QUIT
221 2.0.0 nc-ass-vip.sdv.fr closing connection
```

Cependant, il reste des configuration à réaliser spécifiques à
`sympa`. La documentation les décrit mais il faut bien comprendre la
notion d´aliases. [voir la documentation](https://sympa-community.github.io/manual/install/configure-http-server-spawnfcgi.html)

### Configuration de nginx

À réaliser.
