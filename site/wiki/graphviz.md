  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">versionning</span>

# Graphviz

## Subgraph

One can use `subgraph` toadd group delimited with rectangles.

![The result](/images/graphviz.webp)

```dot
strict digraph {

    rankdir="TB"

    scheduler_1 [shape="record" style="filled" fillcolor="#3366ff", label="{ scheduler | LocalExecutor }" ]
    dags_mount_1 [shape="record" style="filled" fillcolor="#ffff66", label="{ Dags | docker mount }"]

    postgres [shape="record" style="filled" fillcolor="green", label="{Database | Postgres}"]
    webserver_1 [shape="record" style="filled" fillcolor="#ff00ff", label="{ webserver }" ]


    subgraph cluster_1 {
        label="Server";

        dags_mount_1

        subgraph cluster_2 {
            label="Docker";
            style=filled;
            color=lightgrey;

            webserver_1
            scheduler_1
        }

    }

    postgres

    postgres ->  scheduler_1 [dir=none]
    webserver_1 ->  postgres [dir=none]
    scheduler_1 -> dags_mount_1 [dir=none]
}
```
