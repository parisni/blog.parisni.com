  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">monitoring</span>
<span class="w3-tag w3-red tag">web</span>

# Goaccess

This tool is very light and provides a geo-location of ip. It is much lighter
than ELK, and cover the need of having a view on the hit on the web server.

## Install

The real-time rendering works with a web-socket. The installation [is very well
described
here](https://di-marco.net/blog/it/2020-01-02-real_time_goaccess_dashboard_with_nginx/).


```
# wget  https://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
geoip-database /usr/local/etc/goaccess/dbip-city-lite-2021-12.mmdb

# Path where the persisted database files are stored on disk.
# The default value is the /tmp directory.
db-path /usr/local/etc/goaccess/

# Persist parsed data into disk.
persist true

# Load previously stored data from disk.
# Database files need to exist. See `persist`.
restore true

```
