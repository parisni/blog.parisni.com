  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">database</span>

# MySQL

## Connect from cli

```bash
mysql -u <user> -P 3306 -p
```

## Dump the database

```bash
mysqldump -u <user> -P 3306 --all-databases -p > </path/to/file>
```
