  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">os</span>
<span class="w3-tag w3-red tag">linux</span>

# Ubuntu

## Apt

### List installed packages
```
dpkg -l
```

### List installed files
```
dpkg -L <package-name>
```

### Search a package
```
apt-cache search <pattern>
```

## Login

### Prevent suspend when laptop closed

```bash
cat /etc/systemd/logind.conf
[Login]
HandleLidSwitch=ignore
```

## Installer

### Snap

It is well integrated with ubuntu. Sometimes flatpak is more up-to-date but
snap has the advantage to provide a command in the path.

Also, snap does not access to `/tmp` folder, which can be problematic.

### Flatpak

Flatpak is as good as snap, with a different software coverage, but it does not
provide a command in the path: instead `flatpak run <tool>`.

Also the sofware are installed by package name `flatpak install flathub org.gimp.GIMP`


## Dark Mode

### Gtk natives


```bash
cat .gtkrc-2.0
gtk-application-prefer-dark-theme=true
gtk-key-theme-name = "Emacs"
gtk-icon-theme-name = "Yaru-dark"

```

```bash
18:46 $ cat .config/gtk-3.0/settings.ini
[Settings]
gtk-application-prefer-dark-theme=true
gtk-key-theme-name = emacs
gtk-icon-theme-name = "Yaru-dark"
gtk-can-change-accels = 1

```

### Gtk flatpak

[see flatpak overrides](https://itsfoss.com/flatpak-app-apply-theme/)

```bash
sudo apt install ubuntu-session yaru-theme-gnome-shell yaru-theme-gtk yaru-theme-icon yaru-theme-sound
ln -s /usr/share/themes ~/.themes
sudo flatpak override  --filesystem=$HOME/.themes
sudo flatpak override --env=GTK_THEME=Yaru-dark
```
