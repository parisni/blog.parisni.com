  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">pdf</span>

# Evince

## Save current layout

When you click in the outline, this keeps the layout:

```bash
gsettings set org.gnome.Evince allow-links-change-zoom false
```
