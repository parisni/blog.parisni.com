  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">bigdata</span>

# Hbase

## Fixing currupted tables

This will fix corrupted blocks on hbase table. Hbase has a x2 replication factor by default. Even in case the hdfs x3 replication factor has failed, hbase can regenerate.

```bash
# Diagnosis
hbase hbck -details
# Fix
hbase hbck -fix 
```
