

  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">media</span>

# CD

## Ripper

ABCDE let you grab/edit the id3 tags within the `$EDITOR`.

```
abcde -o mp3
abcde -1 -o flac # whole cd in a single file
```

It can be configured to not bother on broken cd:

```bash
# ~/.abcde.conf
CDPARANOIAOPTS="--disable-paranoia"
```

## Mount a mp3 cdrom

```
lsblk # to know the cdrom devince
mount /dev/<devince-id> /mnt/cdrom
```
