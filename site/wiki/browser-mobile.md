  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">web</span>

# Mobile Browser

So far, firefox is the best option on mobile.


## Firefox

So far, the best.

- `+`: bottom navbar
- `+`: handle vpn tls with no error
- `+`: handle well password autofill
- `-`: homepage does not offer a simple page
- `-`: no built-in dark-reader (but plugins do)
- `-`: telemetry (but can be deactivated)

## Kiwi Browser

- `+`: bottom navbar
- `+`: handle well password autofill
- `+`: built-in dark-reader
- `-`: trouble with vpn and tls
- `-`: trouble with some websites

## Brave

so much functionality and noise.
