  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">formatting</span>
<span class="w3-tag w3-red tag">pdf</span>

# Markdown

## Internal link with section

### Relative links

```markdown
![The link description](file.md#the-section-to-replace-space-with-minus-and-lower-case)
```

### Absolute links

```markdown
![The link description](/path/to/the/file.md#the-section-to-replace-space-with-minus-and-lower-case)
```

### Clickable images

This allows to make the image clickable (and allow zooming):


```
[![Alt description](image-link.png)](image-link.png)
```

## Generate papers pdf from markdown

[By using pandoc-citeproc](https://gist.github.com/maxogden/97190db73ac19fc6c1d9beee1a6e4fc8)
