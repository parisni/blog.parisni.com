  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">image</span>

# Image


## EXIF metadata


### Manipulation

Exiftool allows to play with image metadata:
- show: `exiv2 <the image.jpeg>`
- delete: `exiv2 rm <the image.jpeg>`

### Installation:

- Ubuntu : `sudo apt-get install libimage-exiftool-perl` 
- Archlinux:`sudo pacman -S perl-image-exiftool`
