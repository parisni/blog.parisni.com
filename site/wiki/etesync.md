  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">caldav</span>
<span class="w3-tag w3-red tag">security</span>

# EteSync

## etebase

This is a e2e backend written in python. A postgres database stores the django roles, permissions and groups informations.

The users data is stored encrypted in a folder tree. Data depends on the client application : card, tasks, calendar, notes.

Users can change their passwords, however they cannot loose them or never access again to their data.

There is no support for group to share the collections. A workaround consists of creating a dedicated user which can share its collections with a set of users.

## Admin UI

Etebase has it's own admin ui, `<etebase-domain>/admin` which allows to create
users and provide them a password. Later the user can change its password in
the etesync UI.

## WebUi

It decrypts the data provided by the server and allows a set of feature such :

- show/edit calendar
- show/edit tasks
- show/edit cards
- those are added to collections
- collections can be shared with user either rw or ro


## etesync-android

You install it on your device and fill your credentials. It decrypts locally and interfaces with the android tools (calendar, contacts, tasks...)

## etesync-dav

It aims at interface users computer thunderbirds or outlook. It is a binary based on radicale and decrypts the server data locally.

I tested it with vdirsyncer with success.


## etesync-notes

Both a mobile and web app allows editing markdown notes.


## Clients

### Android

All right

### Iphone

Etesync allows to share the collection with icloud. Some troubleshoot :
- contact pictures are likely lost in the sync process
- the etesync "My tasks" collection makes the sync process fail, so remove it.


## Resources

- [the notes webui](https://notes.etesync.com)
- [the caldav webui](https://pim.etesync.com)
- [the etebase docker](https://github.com/victor-rds/docker-etebase)
- [the caldav webui docker](https://github.com/etesync/etesync-web/pull/189/files)
- [the notes webui docker](https://github.com/etesync/etesync-notes/tree/master/docker)
