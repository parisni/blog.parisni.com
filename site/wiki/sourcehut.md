  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">versionning</span>

# Sourcehut

## Start a sourcehut project

Init the repository:

1. add your rsa pub key in the profile
2. create a repository `test-git-mail`
3. create a mailing list associated `test-git-mail`  [accessible from here](https://lists.sr.ht/~parisni/test-git-mail)
4. `git clone git@git.sr.ht:~parisni/test-git-mail`
5. init the repos with something and `git push` an initial commit


## Send/apply patches

Send a patch :

1. install and configure `git send-mail` [from the tutorial](https://git-send-email.io/)
2. clone the repository locally
3. configure the local sendmail `git config sendemail.to "~parisni/test-git-mail@lists.sr.ht"`
4. add commit, and then `git send-email HEAD^` or `git send-email qefsfhjkh asdfhqe` for multiple commits


Receive a patch :

1. [configure aerc](https://drewdevault.com/2020/04/20/Configuring-aerc-for-git.html?hmsr=joyk.com)
2. get email and apply patch with aerc keys-map
3. push commits


Ask for motifications :

1. answer to the email patch


Send a modified patch :

1. fix the errors
2. `git commit --amend`
3. `git send-email --annotate -v2 HEAD^`
