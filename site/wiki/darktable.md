
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">image</span>

# Darktable

## darktable from graphic interface

### Create a style
One can create a pipeline of transformations and save them as
re-usable **style**.

## darktable from command line
**darktable-cli** is available together with **darktable** gui version.

### Apply a style to images
[cli documentation](https://www.darktable.org/usermanual/en/overview_chapter.html#darktable_cli_commandline_parameters)

this will apply "style name" and scale the image to 300px
```bash
dakrtable-cli <input file> <outpout file> --style <style name> --width 300
```

this can also apply to folders
```bash
dakrtable-cli <input folder> <outpout folder>/<file template name>.<extension> --style <style name> --width 300
```


## darktable from lua

[The lua api](https://www.darktable.org/lua-api/) offers the darktable features within a lua program.
