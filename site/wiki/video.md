
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">audio</span>

# Vidéo


## Yt-dlp

```
# list subs
yt-dlp -F --list-sub https://www.arte.tv/fr/videos/075801-000-A/flee/
# download subs
--write-subs --sub-langs "fr" https://www.arte.tv/fr/videos/075801-000-A/flee/
```

Resulting vtt files can be merged simply by adding content from one into the other.

Peertube hanlde both vtt and srt.
