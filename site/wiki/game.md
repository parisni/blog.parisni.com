  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">game</span>

# Game

## Neogeo

- [roms download site](https://www.winkawaks.org/roms/neogeo/)
- [rom download site](https://www.planetemu.net/machine/mame)

Download [the neogeo bios
files](https://ia601507.us.archive.org/view_archive.php?archive=/25/items/mame-0.231-merged/neogeo.7z)
and zip them and put into `/usr/local/share/game/mame/roms/`

[Key binding](https://docs.mamedev.org/usingmame/defaultkeys.html)
