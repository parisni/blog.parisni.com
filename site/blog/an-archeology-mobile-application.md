

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-03-28</time></span>
<span id="reading-time" style="visibility:hidden;"/>

# An archeology mobile application

## There is a tool I need
I regularly find ancient artefacts (mostly prehistoric silex). I would
find highly valuable to store the GPS coordinate of those, and get a
map. 

I know this practice is subject to regulation. However, most of the
time, I found the stuff in ploughed fields or in the forest trails. My
guess is having the tool to store and share the information could help
the research by building an open-database.

Such a tool could be free software, accesible to anybody. Some feature
will allow collaboration (annotation) and also contact between
users. The dataset (including photos) will be released as open-data.

## Features
- create an account
- upload a geologalized photo
- show artifacts on a map
  - filter by category
  - filter by user
- add metadata
- suggest metadata
- export open-data

## Implemention
- python API (fastapi)
- postgresql database, postgis
- object storage (store the images)
- imagemagic tools
- android client application
- website client application
