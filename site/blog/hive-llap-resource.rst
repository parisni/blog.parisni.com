.. title: Hive llap resource consumption
.. slug: hive-llap-resources
.. date: Sep 07, 2018
.. link:
.. description:
.. tags: hive, big-data
.. category: data engineering


.. raw:: html

	 <span>Date:<time id="post-date">2018-09-07</time></span>
	 <p>Estimated Time:<span id="reading-time"/></p>

Hive Llap
=========

Resource consumption
--------------------

Hive llap is quite complicated to tune.

.. END_TEASER

.. code-block:: bash

        hive.llap.daemon.task.scheduler.enable.preemption=true


`a great article <https://community.hortonworks.com/articles/149486/llap-sizing-and-setup.html>`_
