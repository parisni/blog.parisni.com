.. title: Spelling, mutt & vim
.. slug: mutt-vim
.. date: Sep 03, 2018
.. link:
.. description:
.. tags: mutt, vim, email
.. category: tools

.. raw:: html

	 <span>Date:<time id="post-date">2018-09-03</time></span>
	 <p>Estimated Time:<span id="reading-time"/></p>

Spelling, mutt & vim
====================

Having a correct spelling is important in society. This is true specially when
come time to write emails.

.. END_TEASER

Here is a trick to activate vim spelling while editing email in **mutt**. Add
the following line into your .muttrc configuration file:

.. code:: bash

        set editor="vim -c 'set textwidth=72' -c':set spell' -c':setlocal spell spelllang=en_us'"

This allows to check English spelling correctly.

Enjoy !
