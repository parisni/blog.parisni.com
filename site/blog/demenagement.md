

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-11-25</time></span>
<span id="reading-time" style="visibility:hidden;"/>

# Aujourd'hui: je déménage demain

Je déménage dans 3 mois, disons sur la planete Mars. Dans ce billet, je biffe les sociétés
en question, appelons les:

- FRIC: Une société fournissant téléphonie et internet
- CPAFER: Une société fournissant téléphonie et internet, mais aussi
  l'infrastructure réseau
- Abrasif: Une société revendant de la téléphonie et internet pour le compte de
  FRIC et CPAFER
- SUM: Une société fournissant électricité
- OEUF-DES-HAIES: Une société fournissant électricité
- CanalPlouf: Une société de télévision
- Clitoriche: Une société fournissant téléphonie et internet, partenaire de CanalPlouf
- Le service des Empotés: Une administration responsable de la récolte des
  finances
- PapierQ: Cette société m'a été conseillée par l'agence immobilière, et se
  présente comme un organisme financé par des acteurs divers, pour simplifier
  les aspects administratifs des déménagements, en particulier l'energie,
  l'internet et la téléphonie. De plus, ils ont des contrats avec des offres
  exclusives
- STOP-DIRECT: Mon ancienne banque: j'ai dû en changer pour obtenir un prêt
  immobilier ailleurs.

Dans cette aventure, je cherche à me doter d'internet, du téléphone fixe, et de
l'electricité de manière optimale. En quête secondaire, je tente de changer de
banque sans faire de vagues.

## JOUR 1

### Clitoriche
Le site internet de la planete Mars donne des détails sur la mise en place de la
fibre dans la ville ou j'emmenage dans 3 mois. Je commence par signaler au
webmaster un lien mort qui m'empeche de confirmer par moi même l'eligibilté de
mon domicile. Je vois cependant le contact de Clitoriche, société mettant en
œuvre la fibre dans ma ville.

Je les contacte donc avec pour objectif de vérifier mon éligibilité. J'indique
habiter dans un immeuble au numéro 666. La personne me répond que le logement ne
contient qu'un seul habitant et que cela doit être une erreur dans leur
fichier, et qu'une demande de rectification doit être faite avant de souscrire.
Je me frotte les mains avec le sentiment de gagner du temps en évitant des
complications à m'y prendre à l'avance.

## JOUR 2

### Clitoriche
Entre temps, en feuilletant les papiers OEUF-DES-HAIES, je réalise que j'habite au 666B.
Pour éviter d'engager une procédure inutile, je rapelle Clitoriche pour leur
signifier mon erreur. Après avoir expliqué le contexte, la personne au bout du
fil me demande mon code de souscription que j'ai dû recevoir par mail. Je suis
formel: je n'ai pas reçu de mail. Pour retrouver mon dossier, je donne mon
adresse email. Il ne lui est pas possible de changer mon addresse pour le 666B.
La personne me conseille alors de rappeler, et de demander le service de
souscription pour résilier mon compte en fournissant mon code.  Je lui fais
remarquer que je n'ai justement pas ce code. La personne me demande de
patienter. Après 10 minutes d'attente, la ligne est coupée.

Je rappelle donc Clitoriche avec une stratégie différente: je demande à
souscrire un nouveau contrat.  Je vais donc au bout de la démarche en précisant
le 666B cette fois, et je reçois enfin l'email, avec le code. C'est un forfait
fibre à 33€/mois, sans télévision, avec une téléphonie payante à la minute pour
les appels sortants: ce n'est pas donné mais après tout, ce sont les seuls à
fournir de la fibre ?

Je ne réalise pas à ce moment là que je ne souhaitais pas prendre de contrat si
tôt mais simplement vérifier mon éligibilité à la fibre.

### PapierQ
Avec un sentiment d'accomplissement, j'enchaine avec PapierQ.

Je souhaite gérer l'electricité avec eux. Très cordial, mon interlocuteur me
conseille de prendre un contrat à taux fixe au plus vite étant donné
l'augmentation du prix de l'energie dûe à la crise du gaz avec les Russes. Il
me fait l'éloge de SUM énergie, et sa filiale Verte. Je refrenne mon envie de
tempérer son engouement vis à vis de SUM, et nous passons à la simulation.
Après vérification, le verdict tombe: le forfait que SUM me propose est de
160€ par mois. Je tombe de si haut, que je ne ressens rien tant le choc est
violent et sec. Nous tombons d'accord pour considérer une autre option, à
savoir payer en fonction de ce qui est consommé. Cela est plus raisonnable, 9€
pour le forfait, 5€ pour l'assurance 24/24 panne électrique, et le
kilowat/heure utilisé. Plutôt enjoué par la tournure des évenements, je lui
demande néammoins si l'assurance est optionnelle, ce qu'il me confirme avec une
comme une gène dans la voix. Je ne me gène pas pour lui dire que j'aimerais
autant ne pas prendre cette assurance.

Avant de se séparer bons amis, il me fait remarquer qu'il peut aussi me faire
une comparaison qui n'engage à rien sur les offres internet. Je suis déjà
engagé avec Clitoriche, mais finalement, pourquoi ne pas bénéficier de leurs bons
conseils voire de leurs offres spéciales ?

Lui n'est pas habilité sur la partie internet et il me passe son collègue pour
lequel il a une estime et une confiance sans borne. Rapidement celui-ci
m'oriente vers FRIC. Après plusieurs confirmations de sa part: au 666B, je suis
bien éligible. Il m'annonce que le forfait disponible est a 20€/mois pendant
1an puis 44€ - mais que je pourrai changer - avec plus de 200 chaines de télé
et la téléphonie complète. Je lui fais part de mon désintéret pour la télé,
mais il n'a pas d'autre forfait à me proposer. Fort bien, cela me fait tout de
même gagner près de 14€ par mois par rapport à Clitoriche. C'est une belle
victoire, et je me réjouis d'annoncer mes prouesses à ma femme quand elle
va rentrer.

Après avoir reçu de moi des explications liriques mais confuses et quelques
recherches de son côté, mon épouse me fait remarquer qu'un forfait FRIC semble
plus adapté et surtout moins cher que ce que j'ai souscrit. Immédiatement,
j'envoie un mail mesuré à PapierQ pour leur indiquer qu'un forfait plus
interessant que ce qu'ils m'ont fait souscrire semble exister, et que je leur
serais gré de me basculer dessus.

## JOUR 3

### Empotés
Je reçois un email de mon ancienne banque STOP-DIRECT qui m'indique que mon
solde est désormais de 0€ et que tout à été envoyé à ma nouvelle banque. Je
suis étonné car le courrier que j'avais reçu quelques jours avant m'indiquait
que je devais leur envoyer un RIB et des justificatifs pour qu'ils procèdent à
ce transfert.  Mais soit, ce qui m'inquiète davantage est ma taxe d'habitation,
qui devait être débitée sur mon ancien compte. Ce n'est pas par choix, j'avais
fait le changement de coordonnées banquaires sur le site du service des
empotés, mais apparement cela n'a pas été pris en compte. Je tente donc de me
connecter aus site de STOP-DIRECT, mais mon compte à été gelé. Au téléphone, le
conseiller m'indique qu'en effet, un débit du service des Empotés est en cours
mais que celui-ci sera refusé, et que cela n'est pas aussi grâve que si il ne
pouvait pas être honoré.

Sur le site du service des Empotés, je cherche par tous les diables la manière
de changer de coordonnées banquaires, et je finis par constater que la prise en
compte des nouveau RIB se fait au moins un mois après son changement.  Je les
contacte pour 1.  me plaindre du fait de ne pas avoir été prévenu au moment de
changer de RIB de la lenteur de ce processus et 2. leur demander comment
procéder pour que le prélèvement soit effectué sur ma nouvelle banque.

Joie: je reçois une réponse dans la journée ! Je me rends sur le site pour
consulter ma messagerie sécurisée. En quelques mots laconiques on me confirme
"qu'en effet le changement de RIB prend un certain temps". Je dois me
concentrer avec énergie pour prendre la sage décision de poliement reformuler
ma question pour cette fois obtenir la marche à suivre pour qu'ils soient
payés, et que je ne devienne pas interdit banquaire.

### PapierQ
Je reçois comme réponse que trop tard, si je veux changer de forfait, je dois
appeler un numéros FRIC (pour la modique somme de 0.35€/min).

## JOUR 5

### Clitoriche
Comme prévu, je contacte Clitoriche avec cette fois, mon numéros de souscription.
Je reçois d'abord des excuses car leurs techniciens on indiqué un problème
sur mon dossier. J'en viens à expliquer mes problèmes à savoir que 1.
l'option téléphone n'est pas suffisante 2. j'ai vu un contrat 40% moins cher
chez eux et 3. je souhaite débuter mes mensualités dans 3 mois.

Concernant l'option téléphone, si je débourse 5€ supplémentaires je pourrai
téléphoner sur les fixes en illimité. Si je débourse 4€ de plus par mois, c'est
aussi vers les mobiles que pourrais appeler sans limite. Cela nous s'élève
désormais à près de 45€/mois.

Concernant le contrat moins cher, j'ai précisé que c'est sur "le réseau CPAFER"
que cette offre s'appuie. En entendant le mot clé CPAFER, mon interlocuteur ne
cache plus sa gène et m'indique que Clitoriche n'est pas CPAFER mais une filiale de
CanalPlouf. Je précise que l'offre est visible en première page sur leur site.
Après vérification, je dois noter le numéro de Clitoriche-CPAFER, une autre filiale.

Concernant la question du début de mes mensualités, on me répond qu'il faut
attendre que mon compte soit débloqué, je pourrai les rappeler pour
avoir des détails. Je fais remarquer que c'est une question générale mais je
réalise que cette considération abstraite n'est pas à la porté de mon
interlocuteur, et je le remercie pour ses réponses précises.

Le site que j'ai sous les yeux propose une simulation d'éligibilité à la fibre.
Je tente ma chance et je vois que je suis éligible à Bouygues, FRIC et une
vente privée, chacun avec un numéro de téléphone. Je décroche mon téléphone
pour appeler le numéro indiquant FRIC. La messagerie automatique m'indique que
je suis sur le service: Abrasif. Un conseiller me répond, je commence par lui
faire part de mon désarois: je pensais appeler FRIC. On me répond qu'Abrasif est
affiliée à FRIC. J'explique mon déménagement, ma prochaine addresse. La
personne vérifie mon éligibilité et le verdicte tombe: Je ne suis pas éligible
à la fibre, encore moins celle de FRIC.

### Le service des Empotés

Je suis notifié par mail qu'une réponse du service des Empotés m'a été faite.
Une ligne laconique m'indique en substance que je peux leur envoyer un chèque.
Cette réponse déclenche une série de questions: quel chéquier (je ne l'ai pas
encore reçu), quelle addresse, quel ordre, quelle guarantie que mon nouveau RIB
ne va pas être prélevé aussi ? Je pense que cette conversation ne fait que
commencer.

### FRIC
Après avoir reçu un mail et un sms de FRIC m'engageant à prendre rendez-vous
avec un technicien pour l'installation de ma ligne, je décide de consulter mon
espace client sur le site internet. À nouveau, je constate que le processus est
figé à la première étape : vérification de l'éligibilité.

J'envisage alors de téléphoner à FRIC au numéro présent dans le mail: le 4242.
Très méfiant lorsque je lis "Gratuit depuis une ligne FRIC", je vérifie sur
plusieurs sites, et je suis convaincu: ce numéro est au prix d'un appel local,
c'est à dire gratuit dans le cadre de mon forfait mobile. En revanche, je
risque d'attendre très longtemps.

La messagerie m'indique mon numéro de client (déduit de mon numéros de mobile).
Un conseiller me répond instantanément. Je lui indique que 1. je me suis trompé
d'offre 2. j'ai un doute sur mon éligibilité fibre et 3. j'aimerais débuter mes
mensualités dans 3 mois.

Concernant mon offre, étant donné que rien n'a été déclenché de leur côté, il
me conseille de me rétracter et pour m'éviter d'avoir à rédiger une lettre, me
propose de le faire de son côté: voilà, c'est fait.

Pour l'éligibilité à la fibre, il vérifie et me confirme que c'est le cas. Je
lui fais part de ma réserve étant donné que le réseau CPAFER n'est pas présent, il
m'explique qu'il s'agit d'un autre réseau: planete Mars fibre qui est associatif,
probablement mis en œuvre par la région. En outre, il m'indique que l'immeuble
d'à côté, le 666B ne l'est pas. Je réalise que le conseiller PapierQ m'a
inscrit par erreur au 666 avec FRIC, et je lui fais part qu'il est probable que
j'habite au 666B. Il me donne un détail sur le 666 qui compte 3 habitant: cela me
conforte dans l'idée que j'habite finalement au 666 et que:

- l'adresse sur le contrat d'electricité est éronné (ce n'est pas le 666B)
- Clitoriche n'a pas le bon nombre d'habitant au 666

À suivre...
