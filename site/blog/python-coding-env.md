
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-07-15</time></span>
<span id="reading-time" style="visibility:hidden;"/>

# Relax with python
Most programmer knows python and like it. I have been working with it
a lot recently, and there is few important caveat to really have good
time.


## Forget vim or emacs: Use a tool called an IDE
I have much affection for both vim and emacs and being currently
writing this post with the latter. I tried my best to setup a full
fledged python with it and I won't do it again. I lost myself in few
tutorials and when in the end I got a result it was far from the
feature a python IDE can offer.

Intellij IDE has a python plugin which turn it into Pycharm. Unlike
vim and emacs, they parse the python code with an AST, which provide
some powerful feature:

- a powerful python debugger !
- find where is used the given object in the project
- batch rename object in the project
- refactor piece of code

It also come with feature we like:

- an authentic vim key binding, including search/replace, tab navigation
- an integrated git manager (as good as magit for emacs)
- search for files and search within files
- a nice dark mode
- pytest integration
- last but not least, it manages most programming languages out of the box


## Manage your python versions with pyenv
Managing multiple versions of python is not trivial, and there is many
ways for this. You can go with the packaged versions of the OS (such
apt), but it won't have new versions and you will end up adding weird
source list and repository. I did use anaconda which is a huge
reptilian mess.

Pyenv is really the silver bullet for this. What it does is provide a
command line for installing (download and compile) every python
version that exist including minors and beta. You can then choose for
each project the one you want, without ANY friction. Pyenv is not a
replacement with virtualenv at all, they actually combine: you build
venv with a given python version provided by pyenv.


## Manage you project environment with direnv
Python is so popular, you end up with many
virtualenv. Activating/deactivating them becomes really painful. And
you will forget to do so, and compromise your pip environment easily.

direnv automates the management of virtualenvs when you navigate in
the linux projects, it activate / deactivate them accordingly. You
never have to do this again.

It has a pyenv binding and it's super easy to specify the python
version you d'like in your venv. This produce a virtualenv folder
called .direnv at the root of your project. In order to make intellij
not considering this folder's python sources as part of the project,
you simply have to "exclude" it with a right click.


## Format your code with black
Python syntax rules are great, but it is not perfect. There is still
freedom in the way every one format there code. Black takes care of
everything. Its main advantage is it formats the same source code the
same way, independently of how it is formatted at the beginning.


## Commit your code with pre-commit
There is many small details that everyone wants: format python,
markdown, xml, yaml... All this can be added BEFORE committing to git
with a git-hook. pre-commit takes care of all, and let you add those
step within a descriptive configuration.


## Test your code with tox
When you test the code you don't only check the source code acts
correctly, you also test its environment works. Tox encapsulates the
tests in order they will work on your computer but also on the other's
one. With tox, you avoid the "it works on my laptop" syndrome.



## Build your changelog with Towncrier
Maintaining a changelog is important and there is several tools out
there. Towncrier simplifies this process, and with few efforts, you
get a clear and maintained changelog.md file.

## Publish your package on pypi with twine
Publishing on pypi can be straightforward when you use the right
tools. Both setuptools and twine are fine for this. You also have to
get several files present. [This
blog](https://pgjones.dev/blog/packaging-without-setup-py-2020) is
helpful for this.
