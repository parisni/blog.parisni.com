
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-06-20</time></span>
<span id="book-year" style="visibility:hidden;">1955</span>
<span id="book-author" style="visibility:hidden;">Philip K. Dick</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;">&starf;</span>

# Au service du Maître

## Immunity

Dick dépeint une société sous l´emprise des télépathes (TP) qui
profitent de leur aptitude à lire dans les pensées pour asservir la
société.

Les bons citoyens n´ont rien à se reprocher et cet état de fait n´est
donc pas un problème. Les discidents sont quant à eux éliminés, et il
est facile pour les TP de remonter les réseaux de résistances avec les
informations lues dans les pensées.

Mais cette faculté va se retourner contre eux, car si aucune pensée ne
peut échapper aux télépathes, toute vérité n´est pas bonne à savoir.

## The chromium Fence

Dick dépeint une société divisée politiquement entre deux mouvements
antagonistes: les puristes et les naturalistes. C´est sur l´hygiène
que ces clans s´opposent, les premiers prônant une humanité aseptisée
tandis que les seconds défendant une animalité assumée.

Le narrateur quant à lui n´entend pas choisir de camp et à ce titre
est persécuté aussi bien par l´un que par l´autre. Jusqu´à la mort il
défendra son libre arbitre et cette radicalité est décrite par ses
bourreau comme quelque chose d´effrayant
