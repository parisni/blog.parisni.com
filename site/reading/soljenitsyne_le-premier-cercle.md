
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-31</time></span>
<span id="book-year" style="visibility:hidden;">1957</span>
<span id="book-author" style="visibility:hidden;">Soljenitsyne</span>
<span id="reading-time" style="visibility:hidden;"/>
<!--
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>
-->


# Le Premier Cercle
Ce roman historique s´inscrit dans la fin du stalinisme. Plusieurs
histoires parallèles semblent converger peu à peu. Les péripéties des
protagonistes sont l´occasion pour Soljenitsyne de discuter ses thèses
critiques sur l´histoire ou de donner des détails avérés ou issus de
l´imaginaire collectif ou du sien.


## La charachka: le goulag des ingénieurs
Dans cette catégorie de goulag on a droit à de la viande et du beurre,
ce qui en fait le moins pire de ce type d´endroit. Ceci-dit, on a le
droit à des visites seulement une fois par an et donc ce qui pèse le
plus sur les prisonniers: pas de femme. On y travaille en principe 12h
par jour et ne sont admis en majorité que les ingénieurs ou divers
savants pour avancer sur des projets d´ordre technologiques et
secrets.

## Les relations hiérarchiques
Le roman décrit en détail les interactions entre des personnages ayant
des statuts dans la société très différents. Entre les ingénieurs et
les externes, entre les gardiens et les responsables du charachka,
entre ministres et Staline, entre son secrétaire personnel et ce
dernier. Dans la plupart des cas, la relation est faussée, sur des
bases de défiance et de crainte.

## Réflexions
### Sur la religion
La question du Christianisme est discuté par les «zeks»
(prisonniers). Est-il nécessaire de suivre la Bible au mot prêt ou
bien peut-on s´accorder des légèretés avec le dogme ?

### Sur l´anarchisme

### Sur la linguistique

### Sur la littérature
Un certain nombre d'écrivains et d'œuvres sont citées dans l'ouvrage;
parfois avec des critiques qui engagent les protagonistes et leur
caractère.

> **Gorki** pense fort justement, mais il est plutôt rébarbatif. **Maiakovski** de même mais il est un peu balourd. **Salkykov-Chtchedrine** est un ami du progrès, certes, mais l'ensemble de ses œuvres est à vous tuer raide d'ennui ; et puis **Tourguéniev**, bridé par son idéal d'aristocrate ; **Gontcharov**, qui se rattache à la naissance du capitalisme en Russie ; **Léon Tolstoï** et ses prises de positions en faveur de la paysannerie patriarcale (leur professeur leur avait déconseillé la lecture des romans de Tolstoï qui sont bien longs, et ne peuvent que troubler la clarté des lumineuses critiques qui leur ont été consacrées); ensuite c'était la revue générale d'absolus inconnus comme **Stepniak-Kravtchinski**, **Dostoïevski**, **Soukhovo-Kobyline**, dont il n'était même pas utile de retenir les titres. Pour cette file étirée au long des années, seul **Pouchkine** brillait comme un beau soleil.

### La transgression
L'escapade en train pour la nature est l'occasion pour deux
personnages clé d'enfreindre les règles et de se rapprocher dans cette
démarche. Cela fait irrésistiblement penser à [1984
d'Orwell](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four),
d'autant que l'homme se met à boiter - détail qui ne semble fait que
pour affirmer ce clin d'œil.

### Sur la peinture

### Sur le progrès

> - En art, par exemple, il n'y a aucun progrès ! Et il ne peut y en avoir !
> - De fait ! De fait ! Et c'est saisissant ! s'extasia Nerjine. Il y a eu un Rembrandt au XVII siècle et on peut toujours esayer aujourd'hui de lui faire la pige ! Qu'était la technique au XVII siècle ? Aujourd'hui elle nous paraît  à la mesure de sauvages. Quelles pouvaient être les innovations techniques dans les années 1870 ? Des amusettes pour enfants. Et c'est dans ces années là qu'on a écrit Anna Karénine. Que peut on me proposer de supérieur ?

### Sur l'amitié

### Sur la condition féminine

### Sur la hiérarchie sociale

> Il faut remodeler la conscience de l'humanité de façcon que les hommes n'aient l'orgueil que du trit manuel et qu'ils rougissent d'être surveillants, ou directeurs, ou pontes d'un parti. Il faut faire en sorte que le titre de ministre soit aussi peu claironnant que celui d'égoutier. Le métier de ministre est indispensable lui aussi, mais infamant. Si une fille épouse un fonctionnaire de l'État, que cette souillure rejaillisse sur toute la famille ! Voilà mon socialisme à moi !
