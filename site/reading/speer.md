<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-04-16</time></span>
<span id="book-year" style="visibility:hidden;">1970</span>
<span id="book-author" style="visibility:hidden;">Albert Speer</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>

# Au coeur du troisième reich

> Il m´est arrivé exactement la même chose qu´aux grands du parti: eux
> gâchaient leur vie de famille par une vie d´apparat, rigidifiée dans
> les attitudes de l´etiquette officielle; moi, au contraire, en
> devenant l´esclave de mon travail.


## Architecte du Reich

Albert Speer centre son récit sur Hitler et en fait le détail
minutieux. On y découvre un homme profondément calculateur et
manipulateur - un arnaqueur en somme. Perfide, procrastinateur et
médiocre à bien des égards, c´est à travers leur passion commune pour
l´architecture - motivée par sa mégalomanie - que les deux hommes se
rejoignent. Génie de l´organisation, Speer est sous le charme de
l´exceptionnel tribun qui exploite ses compétences et son ambition
naive.

À travers ce récit, on découvre l´envers du décors des grandes étapes
de la descente aux enfers de l´Allemagne, réarmement, annexion de
l´Autriche, pacte germano-soviétique, invasion de la Pologne, drole de
guerre...  Sur chacun de ces tournants, Hitler semble en pleine
possession de la situation quoi que cela alimente sa mégalomanie qui
lui coutera défaites après defaite son ambition d´un empire
millérnaire qu´il revait vraisemblablement depuis des décennies.

## Ministre de l´armement

Dans la seconde partie de l´ouvrage, Speer décrit son rôle de ministre
de l´armement et l´approche mise en oeuvre pour industrialier et
donner un second souffle au reich. Les passages suivants issus du
chapitre "improvisation organisee" reflètent les grandes lignes de
l´approche:

> [Walter Rathenau] avait découvert que si les entreprises
> échangeaient leurs connaissances techniques, si on opérait une
> division du travail entre les usines, et si on normalisait et
> standardiait la fabrication, la production pouvait faire un bond
> spectaculaire.

> J´étais consterné à l´idée que les méthodes bureaucratiques puissent
> s´íntroduire dans ma propre création. Je ne cessais d´exhorter mes
> collaborateurs à ne pas établir de documents, à régler les questions
> verbalement ou par téléphone, directement et sans formalités, bref à
> éviter à tout prix de constituer des ¨dossiers¨ pour reprendre le
> jargon administratif.

> Les méthodes que j´appliquais, et qui étaient celles d'une gestion
> économique démocratique, fure également un facteur décisif. Le
> principe consistait à faire confiance jusqu´au bout aux industriels
> reponsables. Cette méthode récompensait leur esprit d´initiative,
> éveillait en eux le sens des responsabilités, stimulait leur esprit
> de décision [...]

> Au fond, j´exploitais cette attitude, fréquente chez les
> techniciens, qui consiste à se consacrer à son travail sans se poser
> de questions. Le rôle du technicien étant apparement dégagé de tout
> aspect moral, il n´y eut pendant longtemps de leur part aucune
> réflexion sur la valeur de leur propre activité.[...] le technicien
> n´était plus en mesure d´apercevoir les conséquences de son activité
> anonyme.

## Commandant des armées

Speer met en parallèle la progressive implication d´Hitler dans les
affaires des généraux et l´accumulation des défaites. Si les premières
victoires éclair ont été aquises avec une approche non conventionnelle
et audacieuse, cette chance du débutant ne se reproduit pas. Hitler,
qui maitrise tous les détails des chars, avions et canons n´a pas la
vision d´ensemble et la connaissance du terrain en particulier dans
les airs et par grand froid. Sa propention à s´entourer de non
spécialistes qui le confortent indéfectiblement dans ses choix
entraineront les catastrophes de Stalingrad où des centaines de
miliers de morts auraient pû être évités.

C´est ce qui motivera la tentative d´assacinat d´Hitler par des
généraux.

## Rapport avec les cadres du Reich

Borman est après Hitler le sujet principal du récit de Speer.  Dans
ses maneuvres continuelles pour manipuler le Fuhrer, Borman n´a de
cesse de saper la confiance de ce dernier envers ses ennemis. En
tenant le rôle de secrétaire du parti ce la lui permet de filtrer et
d´avoir connaissance de tous les dossier.

Gõring prend une grande place dans le récit. Si Speer reconnait qu´il
a eu par le passé une grande influence, il le présente à de multiples
reprises comme désinvesti, décridibilisé et en perdition. Par ses
actions en tant que commandant de l´armée de l´air, puis son attitude
de dénégation de la puissance russe puis allíée sur laqueĺle Hitler
s´est appuyé dans son entêtement, il a eu un rôle prépondérant dans la
chute de l´Allemagne.

Goebels est présenté parmi les cadre les plus lucides sur le sort de
l´Allemagne. Il fait aussi parti des rares cadre diplomés car Hitler
avait une propention à nommer des non-initiés à des postes de haute
responsabilité; par exemple Ribbentrop était marchand de Vin de
formation (ministre des affaires étrangères) tandis que Speer était
architecte (ministre de l´armement).

Himmler est peu cité. Speer a tenté de le ranger à ses côtés mais on
comprends que ce dernier en retour à tenté de l´assaciner en le
confiant à son médecin et ami: Karl Franz Gebhard. Himmler a fait
partie du complot destiné à se débarasser de Speer, avec Gõring et
Borman. Cette tentative de meurtre s´inscrit dans le contexte de la
mise en oeuvre des V2 au debut de 1944 ou Speer tentait d´ameliorer
les conditions de vies des prisoniers de camps. Himmler ayant depuis
1942 des vues sur le ministère de l´armement, on peut comprendre
l´intêret qu´il avait dans la disparition de Speer.

## La débacle

Très tôt Speer prend conscience de la défaite du Reich, c´est à dire à
l´hiver de la campagne de Russie, en 42. Au risque d´être écarté du
pouvoir par Hitler, il ne se cachera jamais de ce constat et n´aura de
cesse de tenter de faire passer le message. Dans les derniers mois,
Speer va combattre la politique de la terre brulée imposée par
Hitler. Pour cette raison, ce dernier le rayera de son testament
politique en faveur de Saur.

On obtient des détails psychologiques précis sur la fin de Hitler et
de Eva Braun. Le premier est anéanti, désincarné, tandis que la
seconde se montre guillerette à l´approche de cette échéance. Il est
très clair qu´elle a choisit ce suicide.

## Sur le banc des accusés

Speer sera en contact étroit avec les Généraux Américains avec qui ils
partagent en particulier sur l´efficacité des méthodes de
bombardements.

Rescapé du tribunal de Nuremberg, Speer niera toute sa vie, et dans ce
livre avoir été conscient du sort des Juifs dans les camps. Hors on
sait depuis qu´il a validé et signé les plans d´Auschwitz. Il ne
pouvait donc pas ignorer l´inconcevable. Le mot Juif apparait à une
seule reprise dans le texte de 800 pages.
