<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-09-04</time></span>
<span id="book-year" style="visibility:hidden;">1965</span>
<span id="book-author" style="visibility:hidden;">Frank Herbert</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;">&starf;</span>

# Dune

# Le livre

```
Il devrait exister une science de la contrariété. Les gens ont besoin
d'épreuves difficiles et d'oppression pour developper leurs muscles psychiques.

Extraits de Les Dits de Muad'Dib, par la Princesse Irulan.
```

# Le film

## Le jeux vidéo

En 1992, sort le jeu Dune, suivit de près par Dune 2. En réalité le premier
opus est porté par une fine équipe française qui porte le projet à bout de bras
de manière clandestine. On peut encore y jouer en récupérant l'iso du cd-rom,
et en installant dosbox sous linux.

C'est une expérience immerssive, notamment grâce à [Spice
Opera](https://www.youtube.com/watch?v=FjHon6yg-r8), la musique de [Stéphane
Picq](https://en.wikipedia.org/wiki/St%C3%A9phane_Picq) et [Philippe
Ulrich](https://fr.wikipedia.org/wiki/Philippe_Ulrich). Ce dernier est à la
fois musicien, programmeur informatique, graphiste, scenariste, producteur.
Outre le jeux [L’Arche du Capitaine
Blood](https://en.wikipedia.org/wiki/Captain_Blood_(video_game)) On lui doit
notamment ce couplé, dans [Un délicieux
Carnage](https://philippeulrich.com/author/philippeulrich/page/2/):

**Mégatonne**

```
Les sanglots longs
Des électrons
De l’atome
Percent mon cœur
D’une chaleur
Mégatonne
Tout suffocant
Et blême quand
Sonne l’heure H
Je me souviens
Des jours anciens Et je FLASHE…
```
