<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-07-04</time></span>
<span id="book-year" style="visibility:hidden;">1930</span>
<span id="book-author" style="visibility:hidden;">Léon Trotsky</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>


# Ma vie

## Une enfance entre la campagne et la ville

T. donne les détails de son enfance. On comprend qu´à l´époque
bénéficier d´une éducation est rare. Il faut que la famille ait les
moyens de les financer. L´appartenance ethnique de la famille a de
l´importance, puisque des quotas sont appliqués pour l´entré dans les
études - en particulier pour les juifs. Enfin, une selection sur les
capacités des élèves est opérée.

L´enfance de T. lui permet d´observer la société de la campagne et de
la ville. L´essentiel de cette enfance consiste à une élévation et une
ouverture intelectuelle, au contact de toujours davantage de maîtres
et mentors. Il lit et écrit très tôt et très fort, encouragé et
corrigé par les adultes mais aussi initié, à travers des veillées de
lectures, et la rédaction de vers.

Les émotions dont il fait état concernent exclusivement les rapports
dominants/dominés et très tôt il prend la défence des faibles et
s´oppose violemment aux puissants:

> [...] sympathie pour les opprimés, indignation contre les injustices

## La politisation

Vers 18 ans, T. commence son initiation à la politique. Les idées
socialistes finissent par le convaincre. C´est d´une part à travers la
lecture des ouvrages tels que *la logique* de [John Stuart
Mill](https://fr.wikipedia.org/wiki/John_Stuart_Mill), *Culture
primitive* de
[Lippert](https://en.wikipedia.org/wiki/Julius_Lippert_(historian)),
l´*Utilitarisme* de
[Bentham](https://fr.wikipedia.org/wiki/Jeremy_Bentham), l´*Histoire
de la révolution francaise* de
[Mignet](https://fr.wikipedia.org/wiki/Fran%C3%A7ois-Auguste_Mignet),
et des journaux et d´autre part par la fréquentation de membres
actifs et militants que T. va façonner ses idées. Plusieurs de ses
écrits de l´époque ne seront pas publiés, critiques littéraire, pièces
de théatre. À force de réunions et de remues-méninges et de
tractations auprès des ouvriers, les premiers résultats vont
arriver...en même temps que les premiers emprisonnements. Cette ardeur
au travail fut déclenchée par cette *métaphore du haricot* dans une
taverne:

> C´est très simple: je met un haricot sur la table, c´est le tsar; autour de lui, d´autres haricots: c´est les ministres, les généraux; ensuite, les nobles, les marchands; et ce tas de haricot, c´est le simple peuple. Et maintenant, je demande: ou est le tsar ? L´orateur montre le haricot du milieu: "Ou sont les ministres?" il montre ceux qui entourent le haricot du milieu.

> C'est comme j'ai dit, reprend-il, et l'autre est d'accord. Mais attend...attends maintenant...  il ferme tout à fait l´oeil gauche. Là, je mêle, de la main, tous les haricots ensembles... et bien, que je dis, ou est le tsar ? Ou sont les ministres ? comment s´y retrouver ? comment s´y retrouver... on ne les voit plus, c´est bien ça, que je dis, on ne les voit plus... il faut seulement mélanger tous les haricots.

## La Révolution perpétuelle

T. va construire ses idées révolutionnaires à travers la lecture,
l´écriture, les voyages dans toute l´Europe mais surtout la prison et
la déportation.

La première révolution à laquelle T. participe est celle de 1905. Elle
se solde par un échec et T. est condamné à nouveau à la déportation.
[soviet](https://en.wikipedia.org/wiki/Soviet_(council))

T. vivra dans divers pays dont la Suède, l´Autriche, la Suisse et
l´Angleterre. Dans chacun d´eux, il mènera la révolution à travers la
rédaction d´articles dans des journaux aux tirages internationaux. Ces
journaux ont eu comme effet de mettre sur pied un réseau de
révolutionnaires gauchistes.

Outre la critique des écrivains et penseurs, T. brosse le portrait
implacable de nombreuses personnes qu´il a rencontrés. Parmi les
centaines de portrait, il s´attarde sur celui de [Jean
Jaures](https://en.wikipedia.org/wiki/Jean_Jaur%C3%A8s) et d´[August
Bebel](https://en.wikipedia.org/wiki/August_Bebel), ses meilleurs
alliés français et allemands, qui vont disparaître avant la révolution
de 1917.

## L´exil aux USA

Tout comme Bardamu dans *voyage au bout de la nuit*, T. va débarquer à New York !

> Un dimanche, le 13 janvier, nous arrivons devant New York. À trois heures du matin, réveil général. Nous sommes en place. Il fait sombre. Il fait froid. Du vent. De la pluie. Sur la berge, un humide amoncellement d´édifices. Le Nouveau Monde... Je me trouvais à New York., cité fabuleusement prosaïque, de l´automatisme capitaliste, où triomphent, dans les rues, la théorie esthétique du cubisme, et , dans les coeurs, la philosophie morale du dollar. New York m´en imposait parce qu´il exprime au mieux l´esprit moderne.

En effet, après avoir été expulsé de France, puis d´Espagne sous
l´impulsion des services secrets Russes, T. est d´abord destiné à
l´exil à Cuba, puis finalement devant son refus catégorique aux
États-Unis.


## Révolution 1917

Immédiatement après la nouvelle de la révolution en cours en Russie,
T. et sa famille embarquent. À l´ambassade, l´Angleterre accorde le
droit de passage aux réfugiés politiques. Cependant, arrivé au Canada,
T. et les autres sont incarcérés pendant un mois comme dangereux
socialistes. Mais le premier gouvernement révolutionnaire russe finit
par demander sa relaxe sous l´impulsion de Lénine.

### Installation à Pétrograd
Pétrograd, alors sous le contrôle Allemand (anciennement St
Petersbourg) est le lieux de ralliement des bolchéviques. T. enchaîne
d´innombrables meetings en opposition au gouvernement révolutionnaire
à tendance libérale. C´est de Petrograd que la révolution Bolchévique
naîtra quelques mois plus tard.

### Guerre civile




Après avoir repris le pouvoir aux mains de [Kerensky](https://en.wikipedia.org/wiki/Alexander_Kerensky) la révolution s´est heurté à de nombreuses difficultés. En premier lieu la première guerre mondiale dans laquelle elle était engagée en particulier face à l´Allemagne et l´Autriche. T. était alors en charge des affaires étrangères et il a appliqué une stratégie de temporisation avec pour objectif de propager la révolution dans les pays de l´Est. Le [traité de Brest-Litovsk](https://fr.wikipedia.org/wiki/Trait%C3%A9_de_Brest-Litovsk) fut une véritable tribune pour le communisme, ou était dénoncée la guerre des bourgeois sur le dos du prolétariat. Les bolchéviques refusèrent de signer la paix afin de ne pas ternir les idéaux révolutionnaires. Après l´echec des négociations et la reprise du front, T. démissionna et fut nommé à la tête de l´[armée rouge](https://fr.wikipedia.org/wiki/Arm%C3%A9e_rouge), où il fit preuve de poigne et motiva les hommes en les imprégnant de discours communistes. Enfin, la [guerre civile russe](https://fr.wikipedia.org/wiki/Guerre_civile_russe), largement entretenue par l´Angleterre, la France et la Finlande accapara T. qui à bord [de son train](https://en.wikipedia.org/wiki/Trotsky%27s_train) pendant deux ans et demi parcouru les 14 fronts dans l´Ouest et au sud de la Russie face à  [l´armée blanche](https://fr.wikipedia.org/wiki/Arm%C3%A9es_blanches).

T. insiste et donne tous les éléments pour prouver sa bonne entente avec Lénine.
Il démontre en disséquant chaque étapes de la mise sur pieds du gouvernement bolchévique que tous deux étaient sinon toujours d´accord sur les choix à faire, toujours dans une entente honnête et ouverte à la recherche de compromis toujours meilleurs.
À l´inverse, il décrit avec des détails croissants dans le récit les manipulations de Staline.
Il raconte à quel point Lénine avait perdu confiance et se méfiait de Staline jusqu´à donner ses dernières forces dans sa lutte pour l´évincer du pouvoir.
Par malchance, l´état de santé de Lénine l´empêchât d´aller au bout de son objectif comme la suite de l´histoire de l´URSS le prouve.
T. a été prévenu et était conscient des complots de Staline. Il a fait le choix de conserver son énergie pour la révolution; là où Staline mettait tous ses efforts à rassembler et mettre en place le maximum d´ennemis contre Trotsky.

### La Troïka

Afin d´influencer le bureau lors des votes concernant la gestion du
parti, Staline va constituer une
[troïka](https://en.wikipedia.org/wiki/List_of_leaders_of_the_Soviet_Union#List_of_troikas)
avec Kamenev et Zinoviev. T. les décrit tous deux comme infiniment
supérieurs à Staline politiquement, mais dénués de caractère et de
courage.

La maladie a eu une grande importance dans la prise de pouvoir
progressive et méthodique de Staline. Lénine est emporté par une
maladie artérielle qui le réduit au silence, tandis que Trotsky
souffre de fièvres qui le clouent au lit durant de très longs
mois. Ceci laisse toute la latitude à Staline et ses sbires de réduire
le pouvoir de T. d´abord timidement, puis sans vergogne. T. résume la
situation: les épigones ne pouvant pas s´attaquer directement à T.,
ils mettent en opposition ce dernier et le défunt Lénine.


Dans son titre "Dieu et Juif", Gainsbourg évouque la Troïka des purs
(Trotsky, Kamenev, Zinoviev), qui intervint dans un second temps
lorsque ces deux derniers ont pris conscience de la dangerosité de
Staline.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/d9955baf-e4ca-4db7-bddd-1365edb1320d" frameborder="0" allowfullscreen></iframe>

```text
Le Capital tu as lu de l'Israélite
Karl Marx un beau bouquin
Et le trio bolchevik la troîka des purs eh bien
Tous trois de race sémite
Je te le prouverai tout à l'heure

Dieu est Juif
Juif et Dieu

Grigori Ievseîetch Apfelbaum dit Zinoviev
Lev Borissovitch Rosenfeld dit Kamenev
Lev Davidovitch Bronstein dit Trotsky
```


T. revient sur les raisons de l´échec révolutionnaire et de l´approche
qu´il était la meilleure de prendre. Il semble ne rien regretter:

> voilà pourquoi quand il s´agit de la lutte de grands principes, le
> révolutionnaire ne peut avoir qu´une règle: "fais ce que dois,
> advienne que pourra"

## La déportation et l´exil

Il était vraisemblablement délicat de se débarrasser de Trotsky en
raison du fort soutient ouvrier et de l´opposition en général. Le
[guépéou](https://fr.wikipedia.org/wiki/Guépéou) emmène de force T. et
sa famille dans un village à la frontière de la Chine. En l´espace de
6mois, il reçu et envoya 1500 courriers ou télégrammes. Le parti lui
intima l´ordre de cesser de commander l´opposition.
