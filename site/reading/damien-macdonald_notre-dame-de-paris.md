
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-05</time></span>
<span id="book-year" style="visibility:hidden;">2020</span>
<span id="book-author" style="visibility:hidden;">Damien MacDonald</span>
<span id="reading-time" style="visibility:hidden;"/>

<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;">&star;</span>

# Notre Dame de Paris

Ce roman graphique a la particularité d´être totalement fidèle au
texte de Hugo, ce qui permet d´en apprécier la puissance et la
profondeur.


![](/images/gallery/notredame/medium_0001.webp)

![](/images/gallery/notredame/medium_0002.webp)

![](/images/gallery/notredame/medium_0003.webp)

![](/images/gallery/notredame/medium_0005.webp)

![](/images/gallery/notredame/medium_0006.webp)
