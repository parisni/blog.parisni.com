
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-15</time></span>
<span id="book-year" style="visibility:hidden;">1947</span>
<span id="book-author" style="visibility:hidden;">Antonio Gransci</span>
<span id="reading-time" style="visibility:hidden;"/>

# Lettres de prison

## À Ustica

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=7.800292968750001%2C35.862343734896506%2C15.798339843750002%2C41.43449030894922&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=7/38.703/11.799">View Larger Map</a></small>

Sur cette petite île au nord de la Sicile, Gramsci. se retrouve
emprisonné en compagnie d´autres détenus politique (anarchistes,
communistes, anciens députés). Ils sont mis à l´écart des détenus de
droits communs qui sont traités et dans une situation bien différente
(violence, usurier, alcoolisme).

Afin de ne pas "s´abrutir", G. demande à ses proches des livres, en
particulier de quoi apprendre l´Allemand et le Russe, des livres sur
l´économie, l´histoire et la géographie ainsi que de la littérature
tel que la Divine Comédie de Dante. De plus, des cours sont organisés
pour les détenus politiques auxquels se mêlent quelques villageois.

G. n´a pas tellement à se plaindre de sa situation dans un endroit si
paisible hormis qu´il dort trop peu pour différentes raisons y compris
sa propre nature de petit dormeur.


# À Milan

Après trois mois sur l´île, G. est transféré (en 20 jours!) à la
prison de Milan. À nouveau, il est correctement traité: il peut écrire
deux lettres par semaines, recevoir et lire 8 livre et 7 journaux par
semaine. Il regrette de ne pas pouvoir écrire et prendre des notes ce
qui l´empêche de mener des travaux intellectuels rigoureux.

G. souffre de l´attente des lettres qu´il ne reçois jamais assez
souvent à son goût. Il a par ailleurs de grandes difficultés à écrire
à sa femme et des liens toujours plus forts avec sa belle soeur,
c.-à-d. la soeur de sa femme.

# À Turin
Le transfert de G. prend 19 jours au cours desquelles il décrit une
terrible souffrance analogues au “feu de Saint-Antoine” l'empêchant de
dormir pendant 12 jours. Par la suite, il se retrouve dans une cellule
avec 4 prisonniers politiques malades de la tuberculose. Pour la
première fois, il s'adresse avec véhémence à Tania au sujet de des
initiatives qu'elle prend sans son accord voire avec son désaccord.

À partir de 1929, soit 2 ans(?) après son internement G. obtient
l'accord pour pouvoir écrire dans sa cellule. Il commence par rédiger
des traductions pour se refaire l'esprit.
