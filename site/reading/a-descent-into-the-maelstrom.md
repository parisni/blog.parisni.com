<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-05-31</time></span>
<span id="book-year" style="visibility:hidden;">1841</span>
<span id="book-author" style="visibility:hidden;">Edgar Allan Poe</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>

# A Descent into the Maelström

Amoung short-stories of "Tales of the Grotesque and Arabesque" this
one and "MS. found into a bottle" both depict marvellous nature of
oceans.

Three brothers are caught in a violent storm which train them into a
whirlpool called Maelström. Time is arrested, elements get out of
control, and only the smartest brother save his life.

i felt a parallel with ["the tree little pigs"](https://en.wikipedia.org/wiki/The_Three_Little_Pigs), where the storm would
be the wolf and where each brother adopt a different
strategy. Interstingly in 1840, the first printed versions of the
fable existed in the America.
