<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-06-07</time></span>
<span id="book-year" style="visibility:hidden;">1932</span>
<span id="book-author" style="visibility:hidden;">Louis-Ferdinant Céline</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>

# Voyage au bout de la nuit

## Inapte à la guerre

Céline décrit à travers son narrateur, Ferdinand Bardamu, la guerre
14-18 dans différentes dimensions. Il décrit davantage l´adversité
avec la hiérarchie militaire, et "à l´arrière" dans les hôpitaux
psychiatriques militaires ou au contact des civils. La société tout
entière met la pression sur les soldats pour aller au casse pipe. En
ne parlant pratiquement pas de la guerre et de l'adversaire à
proprement parlé cela en fait un pamphlet contre la guerre.

## Le Congo

Une fois en encore, les compatriotes de Ferdinand en font un bouc
émissaire. Sans raison apparente, c´est toute le pavillon qui
l´embarque pour L'Afrique qui se lie contre lui.

Il s´engage immédiatement pour "tenir une factorie dans la brousse",
métier qu´il exercera bien peu et qui consiste a faire du commerce
avec les autochtones. Il déclare bien vite le paludisme et sera vendu
par ces derniers à un armateur.


## Les États-Unis

### L´arrivée

> Figurez vous qu´elle était debout leur ville, absolument droite. New York c´est une ville debout. On en avait déjà vu nous des villes bien sûr, et des belles encore, et des ports et des fameux même. Mais chez nous, n´est-ce pas, elles sont couchées les villes, au bord de la mer ou sur les fleuves, elles s´allongent sur le paysage, ells attendent le voyageur, tandis que celle-là l´Américaine, elle ne se pâmait pas, non, elle se tenait bien raide, là, pas baisante du tout, raide à faire peur.

C´est en tant que galérien et par surprise que Ferdinand approche les
côtes de New York.

### Les usines ford

Pour gagner sa croûte, F. se rend à Detroit dans les fameuses usine
Ford où "ils prennent tout le monde".

> Une fois rhabillés, nous fûmes répartis en files traînardes, par groupes hésitants en renfort vers ces endroits d´où nous arrivaient les fracas énormes de la mécanique. Tout tremblait dans l´immense édifice et soi-même des pieds aux oreilles possédés par le tremblement, il en venait des vitres et du plancher et de la ferraille, des secousses, vibré de haut en bas. On en devenait machine aussi soi-même à force et de toute sa viande encore tremblotante dans ce bruit de rage énorme qui vous prenait le dedans et le tour de la tête et plus bas vous agitant les tripes et remontait aux yeux par petits coups précipites, infinis, inlassables.


### Le départ

F. rencontre une prostituée qu´il séduit et avec laquelle il passe des
moments inoubliables. Cependant, il rencontre à nouveau Robinson, et
c´est à nouveau le moment pour lui de quitter les États-Unis pour
revenir en France.

> Bonne, admirable Molly, je veux si elle peut encore me lire, d´un endroit que je ne connais pas, qu´elle sache bien que je n´ai pas changé pour elle, que je l´aime encore et toujours, à m manière, qu´elle peut venir ici quand elle voudra partager mon pain et ma furtive destinée. Si elle n´est plus belle, eh bien tant pis ! Nous nous arrangerons ! J´ai gardé tant de beauté d´elle en moi, si vivace, si chaude que j´en ai bien pour tous les deux et pour au moins vingt ans encore, le temps d´en finir.

## Retour en France

### La médecine

À son retour à Paris, F. reprend ses études et se fait médecin de
quartier. Il a toutes les peines du monde à se faire payer.

Il présente la sexualité comme un vecteur de violence qui frappe les
miséreux. La prostitution, les avortements, la masturbation: le
médecin de ville est le témoin et le confident de ces scènes. On voit
comment les familles tentent de l´instrumentaliser pour garder une
place dans la société.

> Quand ils l´avaient tellement battue qu´elle ne pouvait plus hurler, leur fille, elle criait encore un peu quand même à chaque fois qu´elle respirait, d´un petit coup. J´entendais l´homme alors qui disait a ce moment-lá: "Viens toi grande ! Vite ! Vient par lá !"Tout heureux. C´était a la mère qu´il parlait comme ça, et puis la porte d´à côté claquait derrière eux. Un jour, c´est elle qui lui a dit, je l´ai entendu : "Ah ! je t'aime Julien, tellement, que je te boufferais ta merde, même si tu faisais des étrons grands comme ça..."

À nouveau, la rencontre fortuite avec Robinson, sonne la fin de la
récréation. Céline démontre comment la misère entraîne et contraint
les individus dans des péripéties. Eux ne font qu´accepter et tolérer
les méfaits que la vie met à leur disposition.


> J'avais l'habitude et même le goût de ces méticuleuses observations
> intimes. Quand on s´arrête à la façon par exemple dont sont formés
> et proférés les mots, elles ne résistent guère nos phrases au
> désastre de leur décor baveux. C´est plus compliqué et plus pénible
> que la défécation notre effort mécanique de la conversation. Cette
> corolle de chair bouffie, la bouche, qui se convulse á siffler,
> aspire et se démène, pousse toutes espèces de sons visqueux à
> travers le barrage puant de la carie dentaire, quelle punition !
> voilà pourtant ce qu´on nous adjure de transposer en idéal. C´est
> difficile. Puisque nous sommes que des enclos de tripes tièdes et
> mal pourries nous aurons toujours du mal avec le sentiment. Amoureux
> ce n´est rien c´est tenir ensemble qui est difficile. L´ordure elle,
> ne cherche ni à durer, ni à croire. Ici, sur ce point nous sommes
> bien plus malheureux que la merde, cet enragement à persévérer dans
> notre état constitue l´incroyable torture.


### Toulouse
Toujours guidé par Robinson, entraîné dans la nuit, F. débarque à
Toulouse comme en vacances. Robinson et la vielle qu´il a tenté
d´assassiner sont en affaire ensemble pour faire la visite d´un caveau
pour le compte de l´Église. Ce dernier est en outre en passe de se
marier et malgré des affaire florissantes, Robinson n´a de cesse de se
plaindre. Si F. n´a jamais fait état de son amitié pour Robinson, on
comprend à travers la déception qu´il décrit avec précision que
c´était le cas. Cette amitié se basait sur une forme de partage de la
nuit, partage du voyage et des aventures qu´il se rend compte ne plus
ou ne finalement jamais avoir vraiment partagé avec son compagnon.

> Ils en ont des pitié les gens, pour les invalides et les aveugles et
> on peut dire qu´ils en ont de l´amour en réserve. Y en a
> énormément. On peut pas dire le contraire. Seulement c´est
> malheureux qu´ils demeurent si vaches avec tant d´amour en réserve
> les gens. Ça ne sort pas, voilà tout. C´est pris en dedans, ça reste
> en dedans, ça leur sert à rien. Ils en crèvent en dedans, d´amour.


### La psychiatrie
Comme toujours, F. est rattrapé par ses vieilles connaissances rentré
à Paris. Un confrêre lui trouve une place dans un asile psychiatrique
en banlieue.

> Je me tenais au bord dangereux des fous, à leur lisière pour ainsi
> dire, à force d´être toujours aimable avec eux, ma nature.


Il va en particulier apprendre l´anglais au directeur de
l´établissement et cela va transformer son destin. À la manière de Don
Quichotte qui à force de lecture d´histoire de chevalerie va parcourir
le monde, le directeur va tout plaquer pour voyager en Angleterre et y
disparaître. F. se voit alors en charge de l´asile, ce qui ne va pas
tarder à attirer les convoitises de ses compères de Toulouse.

> Dans le cas où nous étions, un homme, un costaud, m´aurait fait
> peur, mais d´elle j´avais rien à craindre. Elle était moins forte
> que moi, comme on dit. Depuis toujours l´envie me tenait de claquer
> une tête ainsi possédée par la colère pour voir comment qu´elles
> tournent les têtes en colère dans ces cas-là. Ça ou un beau chèque,
> c´est ce qu´il faut pour voir d´un seul coup virer d´un bond toutes
> les passions qui sont à louvoyer dans une tête. C´est beau comme une
> belle manoeuvre à la voile sur une mer agitée. Toute la personne
> s´incline dans une vent nouveau. Je voulais voir ça.

### La fête foraine
L´amour maladif et la dépression font mauvais ménage. Et ce n´est pas
une fête artificielle telle que les manèges qui peuvent rattraper ces
maladies.

Dans ce dernier épisode, Céline approfondie les sentiments des
personnages tous plus inadaptés à la vie en société. Cela fait l´effet
d´un vieux mur fait avec des pierres biscornues mal choisies qui se
disloque.
