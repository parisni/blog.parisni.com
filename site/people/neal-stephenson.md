<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-10-13</time></span>
<span id="people-birthyear" style="visibility:hidden;">1959</span>
<span id="people-deathyear" style="visibility:hidden;"></span>
<span id="people-profession" style="visibility:hidden;">SF Writer</span>
<span id="reading-time" style="visibility:hidden;"/>

# Neal Stephenson

- [Neal Stephenson](https://en.wikipedia.org/wiki/Neal_Stephenson)
