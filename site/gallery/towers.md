<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
  font-family: Arial;
}

.header {
  text-align: center;
  padding: 32px;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  padding: 0 4px;
}

/* Create four equal columns that sits next to each other */
.column {
  -ms-flex: 65%; /* IE10 */
  flex: 65%;
  max-width: 65%;
  padding: 0 4px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  width: 100%;
}

/* Responsive layout - makes a two column-layout instead of four columns */
@media screen and (max-width: 800px) {
  .column {
    -ms-flex: 55%;
    flex: 55%;
    max-width: 55%;
  }
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    -ms-flex: 100%;
    flex: 100%;
    max-width: 100%;
  }
}

</style>
<body>

<!-- Header -->
<div class="header">
  <h1>Towers</h1>
  <p></p>
</div>


<div class="row">

  <div class="column">
  <a href="/images/gallery/towers/tower-parnasse.webp">
  <img src="/images/gallery/towers/tower-parnasse.webp"></a>
  </div>

  <div class="column">
  <a href="/images/gallery/towers/tower-bastille.webp">
  <img src="/images/gallery/towers/tower-bastille.webp"></a>
  </div>

</div>

</body>
