<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
  font-family: Arial;
}

.header {
  text-align: center;
  padding: 32px;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  padding: 0 4px;
}

/* Create four equal columns that sits next to each other */
.column {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
  max-width: 50%;
  padding: 0 4px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  width: 100%;
}

/* Responsive layout - makes a two column-layout instead of four columns */
@media screen and (max-width: 800px) {
  .column {
    -ms-flex: 50%;
    flex: 50%;
    max-width: 50%;
  }
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    -ms-flex: 100%;
    flex: 100%;
    max-width: 100%;
  }
}

</style>
<body>

<!-- Header -->
<div class="header">
  <h1>Raised Stones</h1>
  <p></p>
</div>


<div class="row">
  <div class="column">

<a href="/images/gallery/raised-stones/IMG_20211112_145855.webp">
<img src="/images/gallery/raised-stones/IMG_20211112_145855.webp">
  </a>
  </div>
  <div class="column">

<a href="/images/gallery/raised-stones/IMG_20210411_144804.webp">
<img  src="/images/gallery/raised-stones/IMG_20210411_144804.webp">
  </a>
  </div>
  <div class="column">


<a href="/images/gallery/raised-stones/IMG_20211112_144739.webp">
<img  src="/images/gallery/raised-stones/IMG_20211112_144739.webp">
  </a>
  </div>
  <div class="column">

<a href="/images/gallery/raised-stones/IMG_20211112_145107.webp">
<img src="/images/gallery/raised-stones/IMG_20211112_145107.webp">
  </a>
  </div>
  <div class="column">


<a href="/images/gallery/raised-stones/IMG_20211112_145253.webp">
<img src="/images/gallery/raised-stones/IMG_20211112_145253.webp">
  </a>
  </div>
  <div class="column">

<a href="/images/gallery/raised-stones/IMG_20211112_151920.webp">
<img  src="/images/gallery/raised-stones/IMG_20211112_151920.webp">
  </a>
  </div>

  </div>
</div>

</body>
