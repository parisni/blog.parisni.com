<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
  font-family: Arial;
}

.header {
  text-align: center;
  padding: 32px;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  padding: 0 4px;
}

/* Create four equal columns that sits next to each other */
.column {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
  max-width: 50%;
  padding: 0 4px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  width: 100%;
}

/* Responsive layout - makes a two column-layout instead of four columns */
@media screen and (max-width: 800px) {
  .column {
    -ms-flex: 50%;
    flex: 50%;
    max-width: 50%;
  }
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    -ms-flex: 100%;
    flex: 100%;
    max-width: 100%;
  }
}

</style>
<body>

<!-- Header -->
<div class="header">
  <h1>Poulpo</h1>
  <p></p>
</div>


<div class="row">


  <div class="column">
  <a href="/images/gallery/poulpo/IMG_20211121_130255.webp">
  <img src="/images/gallery/poulpo/IMG_20211121_130255.webp" alt="Poulpe rue des Amendiers" style="width=100%" >
  </a>
  </div>

  <div class="column">
  <a href="/images/gallery/poulpo/IMG_20211121_160902.webp">
  <img src="/images/gallery/poulpo/IMG_20211121_160902.webp" alt="Poulpe metro ?" style="width=100%">
  </a>
  <a href="/images/gallery/poulpo/IMG_20211121_160746.webp">
  <img src="/images/gallery/poulpo/IMG_20211121_160746.webp" alt="Poulpe metro ?" style="width=100%">
  </a>
  </div>

  <div class="column">
  <a href="/images/gallery/poulpo/IMG_20211120_162156.webp">
  <img src="/images/gallery/poulpo/IMG_20211120_162156.webp" alt="Poulpe rue de Tolbiac" style="width=100%">
  </a>
  </div>

  <div class="column">
  <a href="/images/gallery/poulpo/IMG_20211120_151722.webp">
  <img src="/images/gallery/poulpo/IMG_20211120_151722.webp" alt="Poulpe boulevard de Charonne" style="width=100%">
  </a>
  </div>

  <div class="column">
  <a href="/images/gallery/poulpo/poulpo-volet.webp">
  <img src="/images/gallery/poulpo/poulpo-volet.webp" alt="Poulpe boulevard de l'Hopital" style="width=100%">
  </a>
  </div>

  Eva, paris 3íème:
  <div class="column">
  <a href="/images/gallery/poulpo/20211216_170150.webp">
  <img src="/images/gallery/poulpo/20211216_170150.webp" alt="Paris 3ième, Eva" style="width=100%">
  </a>
  </div>

</div>

</body>
