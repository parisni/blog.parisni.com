<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-07-18</time></span>
<span id="book-year" style="visibility:hidden;">2020</span>
<span id="book-author" style="visibility:hidden;">Romeu Moura</span>
<span id="reading-time" style="visibility:hidden;"/>

# Les coach agiles sont des jésuites

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/e081ec39-5758-4f98-a483-7445e573f487" frameborder="0" allowfullscreen></iframe>

Via la [systémique](https://fr.wikipedia.org/wiki/Syst%C3%A9mique),
R. postule que les systèmes sont composés de trois éléments:
- entité
- but
- relation

Il ajoute que les systèmes ont des *comportements émergents*, qui ne
sont pas souhaités, ni souhaitables par les entités.

Le but de tout système est de se conserver.

L´effet d´une entité peut être imperceptible mais du point de vue
systémique être très fort. Il peut être fort du point de vue
individuel mais imperceptible du point de vue systémique. L´exemple de
la Joconde et de la statue ou du racisme (des milions de petits effets
sont d´un grand effet).

Nuance entre Pouvoir et Contrôle. Le pouvoir s´appuie sur l´influence.

Quelques références:
- [L´utilitarisme - Bentham](https://fr.wikipedia.org/wiki/Utilitarisme)
- [Surveiller et punir - Foucault](https://fr.wikipedia.org/wiki/Surveiller_et_punir)
- [La servitude volontaire - La Boetie](https://fr.wikipedia.org/wiki/Discours_de_la_servitude_volontaire)
