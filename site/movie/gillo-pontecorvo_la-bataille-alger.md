<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-09</time></span>
<span id="book-year" style="visibility:hidden;">1966</span>
<span id="book-author" style="visibility:hidden;">Gillo Pontecorvo</span>
<span id="reading-time" style="visibility:hidden;"/>


# La bataille d´Alger


Le film si longtemps interdit.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/91c55289-9560-4c4c-b1c4-c695b6de66c6" frameborder="0" allowfullscreen></iframe>


Une analyse du film ; de son élaboration à sa diffusion.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/1315bf33-6ff7-4e36-8370-6de80da2b41a" frameborder="0" allowfullscreen></iframe>
