


<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-07-13</time></span>
<span id="book-year" style="visibility:hidden;">2018</span>
<span id="book-author" style="visibility:hidden;">Martin Odersky</span>
<span id="reading-time" style="visibility:hidden;"/>


# Functional programming in scala


The [cheat sheet](https://courseware.epfl.ch/courses/course-v1:EPFL+progfun1+2018_T1/courseware/116bff35543a4932a43dc6ac3602ce74/22f9667e539a47c0aea94b1411afb6d0/1?activate_block_id=block-v1%3AEPFL%2Bprogfun1%2B2018_T1%2Btype%40vertical%2Bblock%404bc0eecec50a4e69b06636fa81f64478) recaps the course.

## List

### More functions on lists

[lecture 5.1](https://www.youtube.com/watch?v=Lww2PZwrH0Q)

### Pairs and Tuples

[lectre 5.2](https://www.youtube.com/watch?v=UULUoj2Itek)

pairs allows pattern matching:

```scala
(x, y) match {
case (Nil, y) => y
case (x, Nil) => x
_ =>
}
```

### Implicit parameters

[lecture 5.3](https://www.youtube.com/watch?v=MFkW6zQQVIw)

It's a good idea to specify *functions arguments* in the additional
parameters, because the compiler can infer the types of the functions
parameters from the regular arguments:

```scala
// no necessary to specify w and z types
def function(x:Int, y:Int)(ord: Ordering):Boolean = {
	ord(x,y) < 0
}
```

Then it is possible to chose an implicit parameter because the
compiler is able to infer the kind of implicit function to look up.

```scala
// no necessary to specify w and z types
def function(x:Int, y:Int)(implicit ord: Ordering):Boolean = {
	ord(x,y) < 0
}
```

### Higher order functions


[lecture 5.4](https://www.youtube.com/watch?time_continue=1&v=McfmstwBYsE)

A high order function takes a other function as argument.

### Reduction of lists

[lecture 5.5](https://www.youtube.com/watch?time_continue=8&v=ZQvLqqYhJ2Y)


```scala
(x, y)) => x * y
// can be rewritten
_ * _
// however
(x, x)) => x * x
// cannot because each _ represents a new parameter
```

### Reasoning about concat


[lecture 5.6](https://www.youtube.com/watch?v=J5v3N149f3g)
