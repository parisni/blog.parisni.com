<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-16</time></span>
<span id="book-year" style="visibility:hidden;">2014</span>
<span id="book-author" style="visibility:hidden;">Ilan Ziv</span>
<span id="reading-time" style="visibility:hidden;"/>

# L'origine du capitalisme

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/80242c69-5a40-459b-b078-c667210d90b8" frameborder="0" allowfullscreen></iframe>

## La théorie de l´économie de marché

[Adam Smith](https://en.wikipedia.org/wiki/Adam_Smith) est considéré
comme celui qui a théorisé et constaté le capitalisme en de 1776 avec
sa publication [La richesse des
Nations](https://fr.wikipedia.org/wiki/Recherches_sur_la_nature_et_les_causes_de_la_richesse_des_nations). Il
s´inspire entre autre de l´école de la physiocratie du médecin
[François
Quesnay](https://fr.wikipedia.org/wiki/Fran%C3%A7ois_Quesnay) qui
cherche à décrire l´économie en mécanismes cycliques comme l´est la
fonction cardio-vasculaire. Ils décrivent donc les flux circulaires
entre producteurs et consommateurs. Pour Smith, l´origine de
l´économie provient de l´inclinaison de l´homme à faire du troc et du
commerce.

Cependant cette théorie est erronée, le mécanisme de "réciprocité" qui
consiste à mettre en commun les productions est davantage en place que
le troc dans les tribus primitives.

## La découverte de l´Amérique

On peut voir les voyages des conquistador comme des entrepreneurs qui
étaient financés par des investisseurs. Les rendements peuvent être
considérables en cas de découvertes. À l´instar des startup actuelles,
beaucoup de ces entrepreneurs échouaient, mais il suffisait qu´un seul
conquiert quelque chose pour que le bénéfices rattrape les
investissements.

Des gens comme
[Cortes](https://fr.wikipedia.org/wiki/Hern%C3%A1n_Cort%C3%A9s)
étaient poussés par leurs dettes pour tenter une prouesse et levaient
des financements de manière désespérés. Une spirale de conquêtes, de
profits, d´investissements et de découvertes s´est débridée.

## Un impact mondial

Les mines d´argents d´Amérique du Sud sont l´occasion pour la Chine de
se tourner avec un grand bénéfice vers une monnaie en argent. Ceci eut
pour effet d´encourager les petits producteurs indépendants. En
revanche, l´effet fut inverse pour ceux d´Europe. En effet le système
des "enclosure" va créer des grandes fermes en fusionnant plusieurs
petites fermes de manière coercitive et mettre ces dépossédés sur la
paille. Avec le supports législatif pour criminaliser la pauvreté, les
gens ont été poussés à devenir de la main d´oeuvre prolétarienne.

## Le commerce triangulaire

Les marchandises allaient d´Europe vers l´Afrique. Des esclaves
allaient entre l´Afrique et l´Amérique. Des denrées depuis l´Amérique
vers l´Europe. Adam Smith était témoins de ces procédés, mais il a
fermé les yeux dans ses théories. En 400 ans d´esclavage, le racisme
est né comme une conséquence, probablement comme manière de supporter
la maltraitance appliquée aux populations.
