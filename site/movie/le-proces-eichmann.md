
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-09-05</time></span>
<span id="book-year" style="visibility:hidden;">1961</span>
<span id="book-author" style="visibility:hidden;">Michael Parzan</span>
<span id="reading-time" style="visibility:hidden;"/>

# Le procès d'Adolf Eichmann

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/6e268acd-6666-4eca-8366-6e0c97802aff" frameborder="0" allowfullscreen></iframe>

Ce procès qui s'est tenu en Israel en 1961 a été largement médiatisé
et retransmit à l'international sur des chaines de télévisions. De
manière interessante, la grande majorité du public, y compris
israelien a découvert les camps d'extermination; soit 15 ans après la
fin de la guerre. C'est dire combien une chappe de plomb a pesé sur
les mémoires. C'est aussi peut-être dû à la nature du média: Primo
Levi avait décrit les camps dès 1947 avec ”Si c'est un Homme”. De même
les procès d'Auschwitz s'étaient tenu en 1952 en Allemagne. Mais la
télévision a _visiblement_ un pouvoir d'information largement supérieur
aux livres, journaux et bouche à oreille.
