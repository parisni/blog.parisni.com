<p>
<div id="raw-meat-index"> </div>
</p>


### Deus Ex Machina
- [Chris Pirih](https://ski.ihoc.net/)

### Databases
- [RDBMS joins](https://blog.dbi-services.com/the-myth-of-nosql-vs-rdbms-joins-dont-scale/)
- [Postgres query optimizer](https://www.vldb.org/pvldb/vol9/p204-leis.pdf)
- [Migrating from postsgres to cockroach](https://storj.io/blog/2020/08/choosing-cockroach-db-for-horizontal-scalability/)
- [laDuckConf](https://invidious.fdn.fr/channel/UCrzp80KZG1K_icINE6D36OA)
- [kappa-architecture](http://milinda.pathirage.org/kappa-architecture.com/)
- [meetup timeseries](https://www.timeseriesfr.org/)
- [pgbackrest](https://pgstef.github.io)
- [tools](https://victorcouste.github.io/data-tools/)

### RSS
- [RSS them all](https://korben.info/avoir-flux-rss-site-web.html)


### Organization
- [Free Software](https://parisni.interhop.org/share/logicielslibres.pdf)
- [wikileaks](https://challengepower.info)
- [forget fediverse](https://forget.zdx.fr/about/)

### jvm
- [Logs](https://stackify.com/compare-java-logging-frameworks/)
- [log4j](https://beuss.developpez.com/tutoriels/java/jakarta/log4j/)
- [scala official tutorial](https://docs.scala-lang.org/overviews/?_ga=2.73685394.1498787618.1594674214-203157230.1594674214)

### Algorithm
- [Big O](https://www.jesuisundev.com/comprendre-la-notation-big-o-en-7-minutes/)
- [Recursion](https://dev.to/christinamcmahon/recursion-explained-with-examples-4k1m) - [Binary Tree Search](https://dev.to/christinamcmahon/understanding-binary-search-trees-4d90)


### Open Data
- [french website scrapped](https://www.steinertriples.fr/ncohen/data/)
- [data.gouv](https://www.data.gouv.fr/fr/datasets/)
- [webmention](webmention.io)
- [peertube search](https://sepiasearch.org/)
- [chatons](https://entraide.chatons.org/en/)
- [Glam wiki](https://outreach.wikimedia.org/wiki/GLAM/Model_projects)
- [Scuttlebutt protocol](https://ssbc.github.io/scuttlebutt-protocol-guide/)
- [freedombox](https://www.freedombox.org/about/)
- [films libres](https://horscine.org/)
- [datasetsearch](https://datasetsearch.research.google.com/)

### Tools
- [tmux tutorial](https://danielmiessler.com/study/tmux/)
- [git](https://www.codeheroes.fr/2020/07/27/git-lutilisation-des-hooks-avec-husky/)
- [federated forge](https://notabug.org/peers/forgefed)
- [UX/UI dashboards](https://github.com/storybookjs/storybook)

### Games
- [Chess on fediverse](https://castling.club/)


### Devices
- [Liseuses](https://www.liseuses.info/73-meilleure-liseuse-pour-lire-du-pdf/)
- [pc portable occasion](https://www.microccase.com/)

### Infra
- [terraform](https://blog.wescale.fr/2020/07/24/demystifions-les-declarations-de-providers-terraform/)
- [gaia-x](https://www.nextinpact.com/article/30352/109041-entre-sac-nuds-et-auto-descriptions-comment-gaia-x-pourrait-revolutionner-multi-cloud)

### Coding
- [sonarqube](https://www.aukfood.fr/testez-votre-code-avec-sonarqube/)
- [rust](https://john-millikin.com/first-impressions-of-rust)
- [quality code brain](https://tobeva.com/articles/brain-oriented-programming/)
- [how does this work](https://adrian.geek.nz/docs.html)
- [mocking python](https://pacmiam.tuxfamily.org/articles/surcharger-le-module-datetime-pour-les-tests-unitaires-en-python.html)
- [paste](https://0bin.net/)
- [CNIL Guide](https://github.com/LINCnil/Guide-RGPD-du-developpeur)

### Hiking
- [openstreetmap offline](https://osmand.net/)
- [primitive tech](https://primitivetechnology.wordpress.com/)
- [mooc silex](https://www.fun-mooc.fr/courses/course-v1:upl+142004+session03/about)

<div id='noreact'/>
