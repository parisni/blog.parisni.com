
# To learn about configuring soupalt, visit https://baturin.org/projects/soupault
[settings]
  strict = true
  verbose = false
  debug = false

  build_dir = "build"
  site_dir = "site"

  default_template = "templates/main.html"
  content_selector = "div#content"

  doctype = "<!DOCTYPE html>"

  clean_urls = true

  page_file_extensions = ["htm", "html", "md", "rst", "org"]

  ignore_extensions = ["draft"]

[preprocessors]
  md = 'cmark --unsafe'
  rst = 'docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/latex'
  org = 'docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/latex'

[index]
  index = true

  index_title_selector = ["h1#post-title", "h1"]
  index_date_selector = ["time#post-date"#, "time", "time#git-timestamp"
  ]
  index_excerpt_selector = ["div#content", "p#post-excerpt", "p"]

  newest_entries_first = true

  extract_after_widgets = ["insert-reading-time", "last-modified", "insert-react"]
  strip_tags = false
  dump_json = "index.json"
  use_default_view = false

[index.views.gallery]
  index = true
  index_selector = "#gallery-index"
  index_processor = "scripts/gallery.py"
  index_title_selector = "h1"

[index.views.readings]
  index_selector = "#readings-index"
  index_item_template = """
   <strong><a href="{{url}}">{{title}}</a></strong> (by <strong>{{book_author}}</strong> in <strong>{{book_year}}</strong>) {{#ystars}}<span style='color:yellow;'>{{.}}</span>{{/ystars}}{{#stars}}{{.}}{{/stars}}
    </br><strong>Published on:</strong> {{date}}.
    <hr class="posts">
  """

[index.views.movie]
  index_selector = "#movie-index"
  index_item_template = """
   <p> <strong><a href="{{url}}">{{title}}</a></strong> (by <strong>{{book_author}}</strong> in <strong>{{book_year}}</strong>) </p>
    <p><strong>Published on:</strong> {{date}}.</p>
    <hr class="posts">
  """

[index.views.people]
  index_selector = "#people-index"
  index_item_template = """
   <p> <strong><a href="{{url}}">{{title}}</a></strong>, <strong>{{people_profession}}</strong> (<strong>{{people_birthyear}}-{{people_deathyear}}</strong>) </p>
    <hr class="posts">
  """

[index.views.blog]
  index_selector = "#blog-index"
  index_item_template = """
    {{date}} <a href="{{url}}">{{{title}}}</a>  ({{reading_time}}) </br>
   <!--
   <p> <a href="{{url}}">{{{title}}}</a>  </p>
    <p><strong>Reading time:</strong> {{reading_time}}.</p>
<p><strong>Last update:</strong> {{date}}.</p>
    <p>{{{excerpt}}}</p>
    <a href="{{url}}">Read more</a>
    <hr>
    -->
  """

[index.views.wiki]
  index_selector = "#wiki-index"
  index_processor = "scripts/wiki.py"
  index_title_selector = "h1"

[index.custom_fields]
  reading_time = { selector = "span#reading-time" }
  date_time = { selector = "time#git-timestamp" }
  book_year = { selector = "span#book-year" }
  book_author = { selector = "span#book-author" }
  stars = { selector = "span#stars" }
  ystars = { selector = "span#ystars" }
  tags = { selector = ".tag" , select_all = true }
  img = {selector = "div.column a", select_all = true}
  people_birthyear = { selector = "span#people-birthyear" }
  people_deathyear = { selector = "span#people-deathyear" }
  people_profession = { selector = "span#people-profession" }



[plugins.section-link-highlight]
  file = "plugins/section-link-highlight.lua"

[plugins.reading-time]
  file = "plugins/reading-time.lua"

[plugins.escape-html]
  file = "plugins/escape-html.lua"

[widgets.page-title]
  widget = "title"
  selector = ["h1", "#title", "#post-title"]
  default = "software lobotomy"
  append = " &mdash; software lobotomy"

[widgets.footnotes]
  widget = "footnotes"
  selector = "div#footnotes"
  footnote_selector = ".footnote"
  footnote_link_class = "footnote"
  back_links = true
  link_id_prepend = "footnote-"
  back_link_id_append = "-ref"

[widgets.refman-table-of-contents]
#  page = "reference-manual.html"
  after = "insert-reading-time"
  widget = "toc"
  selector = "#generated-toc"
  min_level = 2
  max_level = 4
  toc_list_class = "refman-toc"
  toc_class_levels = true
  numbered_list = false
  heading_links = true
  heading_link_text = "→ "
  heading_link_class = "here"
  use_heading_slug = true

#[widgets.table-of-contents]
#  #exclude_page = "reference-manual.html"
#  after = "insert-toc"
#  widget = "toc"
#  selector = "#generated-toc"
#  min_level = 2
#  toc_list_class = "toc"
#  toc_class_levels = true
#  numbered_list = false
#  heading_links = true
#  heading_link_text = "→ "
#  heading_link_class = "here"
#  use_heading_slug = true

[widgets.nav-menu]
  widget = "include"
  file = "templates/menu.html"
  selector = "div#nav-menu"

[widgets.highlight-active-link]
  after = ["nav-menu", "footer"]
  widget = "section-link-highlight"
  selector = "div#nav-menu"
  active_link_class = "nav-active"

[widgets.footer]
  widget = "include"
  file = "templates/footer.html"
  selector = "div#footer"

[widgets.remove-footnotes-from-excerpts]
  widget = "delete_element"
  selector = "a.footnote"
  page = "blog/index.html"
  after = "footnotes"

[widgets.insert-reading-time-container]
  section = "blog"
  widget = "insert_html"
  action = "insert_after"
  selector = "h1#post-title"
  html = '<div>Estimated reading time: <span id="reading-time"></span>.</div>'

#[widgets.insert-toc]
#  section = ["wiki", "blog"]
#  widget = "insert_html"
#  action = "insert_before"
#  selector = "h1"
#  html = '<hr><div id="refman"> <div id="refman-sidebar"> <div id="generated-toc"> </div> </div> <div id="refman-main">'

[widgets.insert-reading-time]
  after = "insert-reading-time-container"
  widget = "reading-time"
  selector = "#reading-time"
  content_selector = "div#content"

[widgets.escape-html-in-pre]
  widget = "escape-html"
  selector = ".raw-html"

# Runs the content of <* class="language-*"> elements through a syntax highlighter
[widgets.highlight]
  after = "escape-html-in-pre"
  widget = "preprocess_element"
  selector = '*[class^="language-"]'
  command = 'highlight -O html -f --syntax=$(echo $ATTR_CLASS | sed -e "s/language-//")'


#[widgets.goat-counter]
#  profile = "live"
#  widget = "insert_html"
#  html = '<script data-goatcounter="https://soupault.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>'
#  selector = "body"
#  action = "append_child"

# Inserts a container for the "last-modified" widget
# Some pages have handwritten timestamps to reflect the time
# of the last significant modification.
# They have the timestamp in <div id="revision"> and should not
# have autogenerated timestamps.
# The plugin inserts a <div id="last-modified"> only if a page
# doesn't have <div id="revision">
[widgets.timestamp-fixup]
  widget = "git-timestamp-container"
  selector = "div#content"
  revision_selector = "div#revision"

# Extracts last modification date of a page from the git log
# and inserts into the element with id="git-timestamp"
# (can be any element with that id)
[widgets.last-modified]
  after = "timestamp-fixup"
  widget = "exec"
  selector = "#git-timestamp"
  command = "git log -n 1 --pretty=format:%ad --date=format:'%Y-%m-%d %H:%M' -- $PAGE_FILE"


[plugins.git-timestamp-container]
  file = "plugins/timestamp-fixup.lua"

[widgets.insert-react]
  after = "timestamp-fixup"
  widget = "react"
  selector = "div#content"
  noreact_selector = "div#noreact"
  content_selector = "#last-modified"

[plugins.react]
  file = "plugins/react.lua"
