import minify_html
import os
import sys
import logging
from itertools import chain

logger = logging.getLogger()


path = sys.argv[1]
def main(path):
    from pathlib import Path
    paths = Path(path)
    for file in (p.resolve() for p in chain(*[paths.rglob(suff) for suff in ["*.html", "*.css"]])):
        print(f"Processing {file}")
        try:
            with open(file, "r", encoding="utf-8") as html:
                minified = minify_html.minify(html.read(), do_not_minify_doctype=True)
            with open(file, "w", encoding="utf-8") as html:
                html.write(minified)
        except Exception as error:
            logging.error(error)

main(path)
