import subprocess
import re
import datetime
import pandas as pd
import calmap
import numpy as np; np.random.seed(sum(map(ord, 'calmap')))
import os



asset_dir = os.getenv("ASSET_DIR", "assets")
leading_4_spaces = re.compile('^    ')


def get_commits():
    lines = subprocess.check_output(
        ['git', 'log', '--date', 'short'], stderr=subprocess.STDOUT
    ).decode("utf-8").split('\n')
    commits = []
    current_commit = {}

    def save_current_commit():
        title = current_commit['message'][0]
        message = current_commit['message'][1:]
        if message and message[0] == '':
            del message[0]
        current_commit['title'] = title
        current_commit['message'] = '\n'.join(message)
        commits.append(current_commit)

    for line in lines:
        if not line.startswith(' '):
            if line.startswith('commit '):
                if current_commit:
                    save_current_commit()
                    current_commit = {}
                current_commit['hash'] = line.split('commit ')[1]
            else:
                try:
                    key, value = line.split(':', 1)
                    current_commit[key.lower()] = value.strip()
                except ValueError:
                    pass
        else:
            current_commit.setdefault(
                'message', []
            ).append(leading_4_spaces.sub('', line))
    if current_commit:
        save_current_commit()
    return commits

commits = get_commits()
dates = []
for commit in commits:
    # see https://docs.python.org/fr/3.6/library/datetime.html
    dates.append(commit["date"])

sery = pd.Series(np.array(dates), index=np.array(dates))
pdate = pd.DataFrame({'dates':sery}, index=pd.DatetimeIndex(sery.index))
def clean_date(row):
    return pd.to_datetime(row['dates'], format="%Y-%M-%d")

events = pdate.apply(clean_date, axis=1)
events4 = pd.Series(pd.to_numeric(events), index=events.index)

plt = calmap.yearplot(events4, year=2021)
plt.figure.savefig(asset_dir + '/images/activity.png', bbox_inches='tight')
