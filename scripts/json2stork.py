#!/usr/bin/env python3

import sys
import json
import toml
import html2text
import string
import os




base_url = os.getenv("SITE_URL")
build_dir = os.getenv("BUILD_DIR")

index_file = sys.argv[1]

with open(index_file, 'r') as f:
    entries = json.load(f)

res = []
for entry in entries:
    if not entry["nav_path"]:
        continue
    subres = {}
    table = str.maketrans(dict.fromkeys(string.punctuation))
    with open(build_dir + entry["url"] + "/index.html", 'r') as content:
        subres["contents"] = html2text.html2text(content.read()).translate(table)
        subres["url"] =  entry["url"]
        subres["title"] = entry["title"]
        subres["filetype"] = "PlainText"
        res.append(subres)

result = {'input': {"base_directory" : build_dir, "title_boost":"Minimal","stemming":"French","files":res}, "output":{"filename":build_dir + "/stork-index.st","excerpts_per_result":100 }}


tomlobj = toml.dumps(result)
#print(tomlobj)

print(tomlobj)
