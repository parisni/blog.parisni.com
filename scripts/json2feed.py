#!/usr/bin/env python3

import sys
import json
import datetime

import dateutil.tz
import dateutil.parser

from datetime import datetime
from feedgen.feed import FeedGenerator
import os

index_file = sys.argv[1]
tab = sys.argv[2] if len(sys.argv) > 2 else None

base_url = os.getenv("SITE_URL")

default_date = datetime(1970, 1, 1, tzinfo=dateutil.tz.gettz('Etc/UTC'))

feed_id = base_url + "/atom.xml"
feed_title = "Software Lobotomy"
feed_subtitle = "parisni's blog"
feed_logo = base_url + "/images/favicon.webp"
feed_language = "en"
feed_author = "Nicolas Paris"
feed_author_email = "parisni@riseup.net"

def get_date(ds):
    try:
        return dateutil.parser.parse(ds, default=default_date).isoformat()
    except:
        return default_date


fg = FeedGenerator()
fg.id(feed_id)
fg.title(feed_title + (" &mdash; {}".format(tab) if tab else ""))
fg.subtitle(feed_subtitle)
fg.author( {'name': feed_author, 'email': feed_author_email} )
#fg.link( href=, rel='alternate' )
fg.logo(feed_logo)
fg.language(feed_language)


with open(index_file, 'r') as f:
    entries = json.load(f)

tabs = ["blog", "reading", "wiki", "movie", "people", "gallery"] if tab is None  else tab
for entry in entries:
    if not entry["nav_path"] or (entry["nav_path"][0] not in tabs):
        continue

    fe = fg.add_entry()

    url = base_url + entry["url"]

    fe.id(url)
    fe.link(href=url, rel="alternate")
    fe.title(entry["title"])
    fe.content(entry["excerpt"], type='html')
    fe.updated(get_date(entry["date_time"]))


atomfeed = fg.atom_str(pretty=True).decode()

print(atomfeed)
