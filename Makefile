BUILD_DIR := build

# In practice it's in my ~/.local/bin
SOUPAULT := /home/natus/bin/soupault


.DEFAULT_GOAL := all

.PHONY: site
site:
	$(SOUPAULT)

.PHONY: assets
assets:
	rsync -ah --exclude '*.jpeg' --exclude '*.png' --exclude '*.jpg' assets/* $(BUILD_DIR)/


.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)/*

.PHONY: serve
serve:
	python3 -m http.server --directory $(BUILD_DIR) 8001

rss:
	scripts/json2feed.py index.json > $(BUILD_DIR)/atom.xml
	scripts/json2feed.py index.json gallery > $(BUILD_DIR)/gallery/atom.xml
	scripts/json2feed.py index.json people > $(BUILD_DIR)/people/atom.xml
	scripts/json2feed.py index.json blog > $(BUILD_DIR)/blog/atom.xml
	scripts/json2feed.py index.json reading > $(BUILD_DIR)/reading/atom.xml
	scripts/json2feed.py index.json wiki > $(BUILD_DIR)/wiki/atom.xml
	scripts/json2feed.py index.json movie > $(BUILD_DIR)/movie/atom.xml

stork:
	./scripts/json2stork.py index.json > stork-index.st
	RUST_BACKTRACE=1 stork --build stork-config.toml

upload:
	aws s3 cp build/ s3://blog.parisni.com --recursive

.PHONY: webp
webp:
	./scripts/yoga-img.py assets/

activity:
	rm assets/images/activity.webp
	python scripts/activity.py

sync:
	rsync -ah  $(BUILD_DIR)/ $(SITE_DIR) --delete

minify:
	python3 scripts/minify.py build/

.PHONY: all
all: clean activity webp assets site minify rss stork sync
